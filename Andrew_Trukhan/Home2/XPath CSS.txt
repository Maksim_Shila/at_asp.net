﻿На странице google.com 
1. line_for_input_keyword_for_search_on_the_page_google.com 
XPATH: .//input[@title='Поиск'] 
CSS: input[title='Поиск']
2. button_"Поиск в Google" 
XPATH: .//input[@value="Поиск в Google"][@type="submit"] 
CSS: input[value='Поиск в Google'][type='submit']
3. button_"Мне повезет" 
XPATH: .//input[@value="Мне повезёт!"][@type="submit"] 
CSS: input[value='Мне повезёт!'][type='submit']

На странице результатов гугла:
1. all_links_to_search_results 
XPATH: .//*[@id='search']//*[@class='r']/a 
CSS: #search*[class='r'] a
2. fifth_letter_"o"_about_in_the_Goooooooooogle 
XPATH: .//*[@id='foot']//*[@id='nav']//*[@aria-label='Page 5']/span 
CSS: #foot #nav td a[aria-label="Page 5"]span

На странице yandex.com:
1. login_input_field 
XPATH: .//div[@aria-label='Авторизация']//input[@name='login'] 
CSS: div[aria-label='Авторизация'] input[name='login']
2. password_input_field 
XPATH: .//div[@aria-label='Авторизация']//input[@name='passwd'] 
CSS: div [aria-label='Авторизация'] input[name='passwd']
3. "Enter"_button_in_login_form 
XPATH: .//div[@aria-label='Авторизация']//button//span[text()='Войти']/ancestor::button
CSS: div[aria-label='Авторизация'] button

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. link_"Входящие" 
XPATH: .//div//a//span[text() = 'Входящие']/ancestor::a 
CSS: div a[title ~= 'Входящие']
2. link_"Исходящие" 
XPATH: .//div//a//span[text() = 'Исходящие']/ancestor::a 
CSS: div a[title ~= 'Исходящие']
3. link_"Спам" 
XPATH: .//div//a//span[text() = 'Спам']/ancestor::a  
CSS: div a[title ~= 'Спам']
4. link_"Удаленные" 
XPATH: .//div//a//span[text() = 'Удалённые']/ancestor::a 
CSS: div a[title ~= 'Удалённые']
5. link_"Черновики" 
XPATH: .//div//a//span[text() = 'Черновики']/ancestor::a 
CSS: div a[title ~= 'Черновики']
6. button_"Новое письмо" 
XPATH: .//a[@title='Написать (w, c)'] 
CSS: a[title='Написать (w, c)'] 
7. button_"Обновить" 
XPATH: .//div[@title='Проверить, есть ли новые письма (F9)']
CSS: div[title='Проверить, есть ли новые письма (F9)']
8. button_"Отправить" (на странице нового письма)
XPATH: .//*[@data-nb='button'][@title='Отправить письмо (Ctrl + Enter)']
CSS: button[title='Отправить письмо (Ctrl + Enter)']
9. button_"Пометить как спам" 
XPATH: .//a[@title = 'Спам']
CSS: a[title = 'Спам']
10. button_"Пометить прочитанным" 
XPATH: .//*[@title="Прочитано (q)"]
CSS: [title="Прочитано (q)"]
11. button_"Переместить в другую директорию" 
XPATH: .//*[@title="В папку (m)"]
CSS: [title="В папку (m)"]
12. button_"Закрепить письмо" 
XPATH: .//*[@title="Закрепить)"]
CSS: [title="Закрепить"]
13. selector_for_search_letter_unique 
XPATH: .//span[@title = 'Тест']/ancestor::a

На странице яндекс диска
1. button_load_files 
XPATH: .//*[@title = 'Загрузить файлы']/input
CSS: [title = 'Загрузить файлы'] input
2. selector_for_unique_file_on_disk 
XPATH: .//div[@class = 'b-content']//div[@title = '03. The Jumping Cats - Crocodile Goes For Porto.mp3']
CSS: div[class = 'b-content'] div[title = '03. The Jumping Cats - Crocodile Goes For Porto.mp3']
3. button_download_file 
XPATH: .//button[@title = 'Скачать']
CSS: button[title = 'Скачать']
4. button_delete_file 
XPATH: .//button[@title = 'Удалить']
CSS: button[title = 'Удалить']
5. button_in_a_basket 
XPATH: .//div[@class = 'b-content']//*[@title = 'Корзина']
CSS: div[class = 'b-content'] [title = 'Корзина']
6. button_restore_file 
XPATH: .//button[@data-click-action="resource.restore"]
CSS: button[data-click-action="resource.restore"]
