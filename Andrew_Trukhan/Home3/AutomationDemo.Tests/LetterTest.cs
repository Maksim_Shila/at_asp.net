﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class LetterTest : BaseTest
    {
        LoginService loginService = new LoginService();
        MailBoxService mailBoxService = new MailBoxService();
        CreateAndSendLetterService createAndSendLetterService = new CreateAndSendLetterService();
        IncomingLettersService incomingLettersService = new IncomingLettersService();
        OutgoingLettersService outgoingLettersService = new OutgoingLettersService();
        BasketPageService basketPageService = new BasketPageService();


        [Test]//тест проверки отправки письма
        public void SendLetterTest_Methods_IsLetterFromLetterFactory_In_Incoming_AND_Outgoing_Letters_Returns_True()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailBoxService.ClickButtonNewLetter();
            createAndSendLetterService.CreateAndSendLetter(LetterFactory.CreateCorrectLetter());

            mailBoxService.ClickButtonIncomingLetters();
            bool letterIsFoundInIncoming = incomingLettersService.IsLetterFromLetterFactory();

            incomingLettersService.ClickButtonOutgoingLetters();
            bool letterIsFoundInOutgoing = outgoingLettersService.IsLetterFromLetterFactory();

            Assert.That((letterIsFoundInIncoming == letterIsFoundInOutgoing), Is.EqualTo(true));
        }

        [Test]//тест проверки удаления в корзину
        public void RemoveLetterInBasketTest_Appeared_True_FromMethod_IsRemovedMailInBasket()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailBoxService.ClickButtonNewLetter();
            createAndSendLetterService.CreateAndSendLetter(LetterFactory.CreateCorrectLetter());
            mailBoxService.ClickButtonIncomingLetters();
            incomingLettersService.RemoveInABasket();
            mailBoxService.ClickButtonBasket();
            
            bool value = incomingLettersService.IsLetterFromLetterFactory();
            Assert.That(value, Is.EqualTo(false));
        }

        [Test]//тест проверки перманентного удаления из корзины
        public void RemoveLetterPermanentTest_Appeared_True_FromMethod_IsRemovedMailFromBasket()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailBoxService.ClickButtonNewLetter();
            createAndSendLetterService.CreateAndSendLetter(LetterFactory.CreateCorrectLetter());
            mailBoxService.ClickButtonIncomingLetters();
            incomingLettersService.RemoveInABasket();
            mailBoxService.ClickButtonBasket();
            basketPageService.RemoveFromBasket();

            bool value = basketPageService.IsLetterFromLetterFactory();
            Assert.That(value, Is.EqualTo(false));
        }
    }


}
