﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class BasketPageService
    {
        private BasketPage basketPage = new BasketPage();
        
        public bool IsLetterFromLetterFactory ()
        {
            return (basketPage.FindLetterFromLetterFactory() != null);
        }

        public void RemoveFromBasket() //удаление из корзины
        {
            NoteCheckBoxLetter();
            basketPage.FindButtonRemovePermanent().Click();
        }

        private void NoteCheckBoxLetter()
        {
            basketPage.FindCheckBoxLetter().Click(); //установка галочки в checkbox
        }
     }
}
