﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class MailBoxService
    {
        private MailBoxPage mailBoxPage = new MailBoxPage();

        public void ClickButtonNewLetter ()
        {
            mailBoxPage.SelectButtonNewLetter().Click();
        }

        public void ClickButtonIncomingLetters()
        {
            mailBoxPage.SelectButtonIncomingLetters().Click();
        }

        public void ClickButtonOutgoingLetters()
        {
            mailBoxPage.SelectButtonOutgoingLetters().Click();
        }

        public void ClickButtonBasket()
        {
            mailBoxPage.SelectButtonBasket().Click();
        }

        public bool IsSelectAnchorInMailBox()
        {
            return (mailBoxPage.SelectAnchorInMailBox() != null);
        }
    }
}
