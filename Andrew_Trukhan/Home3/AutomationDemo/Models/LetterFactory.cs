﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Services.Models
{
    public class LetterFactory
    {
        private const string defaultEmail = AccountFactory.defaultUserEmail;
        public static string defaultTheme = "New letter" + new Random().Next();
        private static string defaultContent = "Content" + new Random().Next();

        private static readonly string wrongEmail = "trukhan.andrew.83yandex.com";

        public static NewLetter CreateCorrectLetter () // для позитивного теста
        {
            return new NewLetter(defaultEmail, defaultTheme, defaultContent); 
        }
    }
}
