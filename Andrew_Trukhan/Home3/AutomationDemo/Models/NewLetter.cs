﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Services.Models
{
    public class NewLetter
    {
        public string Email { get; private set; }
        public string Theme { get; private set; }
        public string Content { get; private set; }

        public NewLetter(string email, string theme, string content)
        {
            Email = email;
            Theme = theme;
            Content = content;
        }
    }
}
