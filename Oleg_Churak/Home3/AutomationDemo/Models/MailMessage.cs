﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Models
{
    public class MailMessage
    {
        public string Recipient { get; }
        public string Subject { get; }
        public string TextMessage { get; }

        public MailMessage(string recipient, string subject, string textMessage)
        {
            Recipient = recipient;
            Subject = subject;
            TextMessage = textMessage;
        }

        public MailMessage(UserAccount userAccount, string subject, string textMessage)
        {
            Recipient = userAccount.Email;
            Subject = subject;
            TextMessage = textMessage;
        }

    }
}

