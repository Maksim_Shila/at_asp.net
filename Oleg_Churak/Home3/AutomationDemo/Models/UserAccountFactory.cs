﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Models
{
    public class UserAccountFactory
    {
        private const string defaultMailUserLogin = "qwerty";
        private const string defaultMailUserPassword = "qwerty";
        private const string defaultUserEmail = defaultMailUserLogin + "@yandex.by";

        private static readonly string WRONG_PASSWORD = defaultMailUserPassword + new Random().Next();
        private static readonly string NONEXISTENT_LOGIN = defaultMailUserLogin + new Random().Next();

        public static UserAccount DefaultAccount
        {
            get { return new UserAccount(defaultMailUserLogin, defaultMailUserPassword, defaultUserEmail); }
        }

        public static UserAccount AccountWithWrongPassword
        {
            get { return new UserAccount(defaultMailUserLogin, WRONG_PASSWORD, defaultUserEmail); }
        }

        public static UserAccount NonExistedAccount
        {
            get { return new UserAccount(NONEXISTENT_LOGIN, defaultMailUserPassword, defaultUserEmail); }
        }

        public static UserAccount ExistedAccountForSending
        {
            get { return new UserAccount(defaultMailUserLogin, defaultMailUserPassword, defaultUserEmail); }
        }
    }
}
