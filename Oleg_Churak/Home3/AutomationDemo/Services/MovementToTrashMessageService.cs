﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class MovementToTrashMessageService
    {

        private SendingMailPage sendingMailPage = new SendingMailPage();
        private ListOfLettersPage listOfLettersPage = new ListOfLettersPage();
        private MainMailStaticPage mainMailStaticPage = new MainMailStaticPage();

        public void MoveMessageToTrash(MailMessage mailMessage)
        {
            mainMailStaticPage.RefreshMailBoxLink.Click();
            listOfLettersPage.LetterCheckbox(mailMessage.Subject).Click();
            mainMailStaticPage.MoveMessageToTrashLink.Click();
        }

        public bool IsFoundMovedToTrashLetter(MailMessage mailMessage)
        {
            mainMailStaticPage.TrashLink.Click();
            mainMailStaticPage.RefreshMailBoxLink.Click();
            bool isResultOnTrashPage = listOfLettersPage.LetterLink(mailMessage.Subject) != null;

            return (isResultOnTrashPage);
        }

    }
}
