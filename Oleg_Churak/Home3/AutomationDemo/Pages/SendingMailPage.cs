﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class SendingMailPage
    {
        private Browser browser = Browser.Instance;

        //private static readonly By sendToRecipientInputByCss = By.CssSelector("div.ns-view-compose-field-to input[type='text']");
        private static readonly By sendToRecipientInputByCss = By.CssSelector("div[name='to']");
        private static readonly By sendToSubjectInputByCss = By.CssSelector("label.ns-view-compose-field-subject input[type='text']");
        private static readonly By contentOfLetterTextAreaByCss = By.CssSelector(".cke_contents_wrap textarea");
        private static readonly By sendingMailButtonByCss = By.CssSelector("div.mail-Compose-Message button");

        internal IWebElement SendToRecipientInput { get { return browser.FindElement(sendToRecipientInputByCss); } }
        internal IWebElement SendToSubjectInput { get { return browser.FindElement(sendToSubjectInputByCss); } }
        internal IWebElement ContentOfLetterTextArea { get { return browser.FindElement(contentOfLetterTextAreaByCss); } }
        internal IWebElement SendingMailButton
        {
            get
            {
                browser.TimeOut(10);
                return browser.FindElement(sendingMailButtonByCss,10);
            }
        }
    }
}

