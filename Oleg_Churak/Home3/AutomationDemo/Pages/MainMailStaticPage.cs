﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class MainMailStaticPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By sendNewMessageLinkByCss = By.CssSelector("a[href='#compose']");
        private static readonly By refreshMailBoxLinkByCss = By.CssSelector("div[data-click-action='mailbox.check']");
        private static readonly By moveMessageToTrashLinkByCss = By.CssSelector("div.ns-view-toolbar-button-delete");

        private static readonly By sentMailLinkByCss = By.CssSelector("a.ns-view-folder[href='#sent']");
        private static readonly By inboxLinkByCss = By.CssSelector("a.ns-view-folder[href='#inbox']");
        private static readonly By trashLinkByCss = By.CssSelector("a.ns-view-folder[href='#trash']");

        private static readonly By userNameDivByCss = By.CssSelector("div.mail-User-Name");

        internal IWebElement userNameLabel { get { return browser.FindElement(userNameDivByCss); } }
        internal IWebElement SendNewMessageLink { get { return browser.FindElement(sendNewMessageLinkByCss); } }
        internal IWebElement MoveMessageToTrashLink { get { return browser.FindElement(moveMessageToTrashLinkByCss, 10); } }
        internal IWebElement RefreshMailBoxLink
        {
            get
            {
                browser.TimeOut(5);
                return browser.FindElement(refreshMailBoxLinkByCss, 10);
            }
        }
        internal IWebElement SentMailLink { get { return browser.FindElement(sentMailLinkByCss, 10); } }
        internal IWebElement InboxLink { get { return browser.FindElement(inboxLinkByCss, 10); } }
        internal IWebElement TrashLink { get { return browser.FindElement(trashLinkByCss, 10); } }

    }
}
