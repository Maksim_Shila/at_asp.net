﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services;
using AutomationDemo.Models;
using NUnit.Framework;

namespace AutomationDemo.Tests
{
    class MovementToTrashMessageTest : BaseTest
    {

        SendingMessageService sendingMailService = new SendingMessageService();
        LoginService loginService = new LoginService();
        MovementToTrashMessageService movementMessageToTrashService =
            new MovementToTrashMessageService();

        [Test]
        public void MovementMessageToTrashTest_CheckingOfMovementInTrash()
        {
            loginService.LoginToMailBox(UserAccountFactory.DefaultAccount);
            var currentMailMessage = MailMessageFactory.SampleMessageToDefaultAccount;
            sendingMailService.SendMail(currentMailMessage);
            movementMessageToTrashService.MoveMessageToTrash(currentMailMessage);
            Assert.IsTrue(movementMessageToTrashService.IsFoundMovedToTrashLetter(currentMailMessage));
        }

    }
}