﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Models
{
    class Letter
    {
        private By letterLocator;
        private static readonly string mailLinkLocator = "//span[@title='{0}']/ancestor::a";
        public string To { get; }
        public string Subject { get; }
        public string Body { get; }
        public Letter(string to, string subject, string body)
        {
            To = to;
            Subject = subject;
            Body = body;
            if (!subject.Equals(""))
            {
                letterLocator = By.XPath(String.Format(mailLinkLocator, subject));
            }
            else
            {
                letterLocator = By.XPath(String.Format(mailLinkLocator, "(Без темы)"));
            }
        }
        public By GetLocator(string subject)
        {
            return letterLocator = By.XPath(string.Format(mailLinkLocator, subject));
        }
        public static Letter GetRandomLetter()
        {
            string to = "nikolaiby1984@yandex.ru";
            string subject = "test" + new Random().Next(0,99999999);
            string body = "the body isn't important" + new Random().Next(0, 99999999);
            return new Letter(to, subject, body);
        }
    }
}
