﻿using Automation.Models;
using Automation.Pages;
using Automation.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation
{
    class LetterTest : BaseTest
    {
        LoginService loginService = new LoginService();
        MailService mailService = new MailService();
        MailPage mailPage = new MailPage();
        private Letter letter;

        [Test]
        public void LetterTest_SendLetter()
        {
            letter = Letter.GetRandomLetter();
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(letter);
            mailPage.RefreshPage();
            mailService.OpenSendPage();
            bool sendLetter = mailService.CheckInSend(letter);
            Assert.True(sendLetter);
            mailService.OpenInboxPage();
            bool comeLetter = mailService.CheckInInbox(letter);
            Assert.True(comeLetter);
        }


        [Test]
        public void LetterTest_SendLetterWithoutFieldTo()
        {
            letter = new Letter("", "WithoutFieldTo", "LetterTest_SendLetterWithoutFieldTo");
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(letter);
            bool errorFieldTo = mailService.CheckErrorWithoutFieldTo();
            Assert.True(errorFieldTo);
        }

        [Test]
        public void LetterTest_SendLetterInTrash()
        {
            letter = Letter.GetRandomLetter();
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(letter);
            mailPage.RefreshPage();
            mailService.OpenSendPage();
            mailPage.RefreshPage();
            mailService.TrashMail(letter);
            mailPage.RefreshPage();
            mailService.OpenTrashPage();
            bool trashLetter = mailService.CheckInTrash(letter);
            Assert.True(trashLetter);
        }

        [Test]
        public void LetterTest_PermanentRemovalOfTheLetter()
        {

            letter = Letter.GetRandomLetter();
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(letter);
            mailPage.RefreshPage();
            mailService.OpenSendPage();
            mailPage.RefreshPage();
            mailService.TrashMail(letter);
            mailService.OpenTrashPage();
            mailService.OpenTrashPage();
            mailService.PermanentRemoval(letter);
            mailService.OpenTrashPage();
            bool trashLetter = mailService.CheckInTrash(letter);
            Assert.False(trashLetter);
        }
    }
}
