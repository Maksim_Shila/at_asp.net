﻿using Automation.Models;
using Automation.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation
{
    [TestFixture]
    class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();

        private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

        [Test]
        public void LoginTest_EmptyLoginOrPassword()
        {
            loginService.EmptyLoginOrMailBox();
            bool actualErrorMessage = loginService.EmptyLoginOrPassword();
            Assert.True(actualErrorMessage);
        }

        [Test]
        public void LoginTest_LoginInAccount()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            bool loginPassCorrect = loginService.CorrectLoginAndPassword();
            Assert.True(loginPassCorrect);
        }

        [Test]
        public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.NonExistedAccount);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
        }

        [Test]
        public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.AccountWithWrongPassword);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
        }
        
    }
}
