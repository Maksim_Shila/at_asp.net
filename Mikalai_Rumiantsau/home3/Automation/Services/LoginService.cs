﻿using Automation.Models;
using Automation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services
{
    class LoginService
    {
        private MailLoginPage mailLoginPage = new MailLoginPage();
        public void LoginToMailBox(UserAccount account)
        {
            mailLoginPage.LoginInput.SendKeys(account.Login);
            mailLoginPage.PasswordInput.SendKeys(account.Password);
            mailLoginPage.SubmitButton.Click();
        }
        public void EmptyLoginOrMailBox()
        {
            mailLoginPage.LoginInput.SendKeys("");
            mailLoginPage.PasswordInput.SendKeys("");
            mailLoginPage.SubmitButton.Click();
        }

        public bool EmptyLoginOrPassword()
        {
            return new MailLoginPage().PopupContentAuthError.Displayed;
        }
        public string RetrieveErrorOnFailedLogin()
        {
            return new LoginFailedPage().ErrorMessage.Text;
        }
        public bool CorrectLoginAndPassword()
        {
            return new MailPage().UserAccountName.Displayed;
        }
    }
}
