﻿using Automation.Models;
using Automation.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Services
{
    class MailService
    {
        private MailPage mailPage = new MailPage();
        public void SendMail(Letter letter)
        {
            MailComposePage mailComposePage = new MailComposePage();
            mailPage.ComposeButton.Click();
            mailComposePage.SendToInputLocator.SendKeys(letter.To);
            mailComposePage.SendSubjectLocator.SendKeys(letter.Subject);
            mailComposePage.SendTextLocator.SendKeys(letter.Body);
            mailComposePage.SendButtonLocator.Click();
        }

        public bool CheckErrorWithoutFieldTo()
        {
            return new MailComposePage().SendErorTextFieldToLocator.Displayed;
        }

        public void OpenSendPage()
        {
            mailPage.SendLetterLink.Click();
        }
        
        public bool CheckInSend(Letter letter)
        {
            mailPage.SendLetterLink.Click();
            return new MailSendPage().findTestLetter(letter.GetLocator(letter.Subject)).Displayed;
        }
        public void OpenInboxPage()
        {
            mailPage.InboxLetterLink.Click();
        }
        
        public bool CheckInInbox(Letter letter)
        {
            mailPage.InboxLetterLink.Click();
            return new MailInboxPage().findTestLetter(letter.GetLocator(letter.Subject)).Displayed;
        }

        public void OpenTrashPage()
        {
            mailPage.TrashLetterLink.Click();
        }

        public bool CheckInTrash(Letter letter)
        {
           bool check=false;
           try
            {
                check = new MailTrashPage().FindTestLetter(letter.GetLocator(letter.Subject)).Displayed;
                check = true;
            }
            catch (NoSuchElementException)
            { check = false; }
            return check;
        }
        public bool CheckInPermanentRemoval()
        {
            return new MailTrashPage().AfterDeleteOfTrash.Displayed;
        }
        public void PermanentRemoval(Letter letter)
        {
            MailTrashPage mailTrashPage = new MailTrashPage();
            mailTrashPage.FindTestLetter(letter.GetLocator(letter.Subject)).Click();
            mailTrashPage.DeleteButton.Click();
        }

        public void TrashMail(Letter letter)
        {
            MailSendPage mailSendPage = new MailSendPage();
            mailSendPage.findTestLetter(letter.GetLocator(letter.Subject)).Click();
            mailPage.RefreshPage();
            mailSendPage.deleteButton.Click();
       }
    }
}
