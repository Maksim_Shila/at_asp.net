﻿using Automation.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Pages
{
    class MailPage
    {
        private Browser browser = Browser.Instance;
        private static readonly By userAccountNameByCss = By.CssSelector("div[class=mail-User-Name]");
        private static readonly By composeButtonByCss = By.CssSelector("a[href='#compose']");
        private static readonly By inboxLetterLinkByCss = By.CssSelector("a[data-params='fid=1']");
        private static readonly By sendLetterLinkByXPath = By.XPath("//a[contains(@href, '#sent')]");
        private static readonly By trashLetterLinkByXPath = By.XPath("//a[contains(@href, '#trash')]");

        public IWebElement UserAccountName { get { return browser.FindElement(userAccountNameByCss); } }
        public IWebElement ComposeButton { get { return browser.FindElement(composeButtonByCss); } }
        public IWebElement InboxLetterLink { get { return browser.FindElement(inboxLetterLinkByCss); } }
        public IWebElement SendLetterLink { get { return browser.FindElement(sendLetterLinkByXPath); } }
        public IWebElement TrashLetterLink { get { return browser.FindElement(trashLetterLinkByXPath); } }
        public void RefreshPage()
        {
            browser.RefreshPage();
        }
    }
}
