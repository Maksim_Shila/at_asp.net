﻿using Automation.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Pages
{
    class MailComposePage
    {
        private Browser browser = Browser.Instance;
        private static readonly By sendButtonLocatorByCss = By.CssSelector("button[title='Отправить письмо (Ctrl + Enter)']");
        private static readonly By sendToInputLocatorByXPath = By.XPath("//div[@name='to']");
        private static readonly By sendSubjectLocatorByCss = By.Name("subj");
        private static readonly By sendTextLocatorByCss = By.CssSelector("div[role = textbox]");
        private static readonly By sendErorTextFieldToLocatorByCss = By.CssSelector(".ns-view-compose-field-to-error");
        

        public IWebElement SendButtonLocator { get { return browser.FindElement(sendButtonLocatorByCss); } }
        public IWebElement SendToInputLocator { get { return browser.FindElement(sendToInputLocatorByXPath); } }
        public IWebElement SendSubjectLocator { get { return browser.FindElement(sendSubjectLocatorByCss); } }
        public IWebElement SendTextLocator { get { return browser.FindElement(sendTextLocatorByCss); } }
        public IWebElement SendErorTextFieldToLocator { get { return browser.FindElement(sendErorTextFieldToLocatorByCss); } }

    }
}
