﻿using Automation.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Pages
{
    class MailTrashPage
    {
        private Browser browser = Browser.Instance;
        private static readonly By afterDeleteOfTrashByCss = By.CssSelector(".mail-NotFound-Title");
        private static readonly By deleteButtonByXPath = By.XPath("//div[contains(@title, 'Удалить (Delete)')]");
        public IWebElement AfterDeleteOfTrash { get { return browser.FindElement(afterDeleteOfTrashByCss); } }
        public IWebElement FindTestLetter(By str) { return browser.FindElement(str); }
        public IWebElement DeleteButton { get { return browser.FindElement(deleteButtonByXPath); } }
    }
}
