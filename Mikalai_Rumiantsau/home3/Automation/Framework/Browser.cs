﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace Automation.Framework
{
    class Browser
    {
        //private const string chromeDriverPath = "D:/chromedriver_win32/chromedriver.exe";
        private static readonly TimeSpan implicitlyWait = TimeSpan.FromSeconds(20);
        private static readonly TimeSpan pageLoadWait = TimeSpan.FromSeconds(20);
       // private static readonly TimeSpan scriptTimeout = TimeSpan.FromSeconds(5);
       
        private IWebDriver driver;
        
        private static Lazy<Browser> instanceHolder = new Lazy<Browser>(() => new Browser());

        public static Browser Instance
        {
            get { return instanceHolder.Value; }
        }

        private Browser() { }

        public Browser Start()
        {
            // It isn't good practice to init and setup driver here this way.
            // Better use driver factories, config files etc. But for our purposes we can leave it for now.
            driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitlyWait(implicitlyWait);
            driver.Manage().Timeouts().SetPageLoadTimeout(pageLoadWait);
            driver.Manage().Window.Maximize();
            return this;
        }

        public void Close()
        {
            if (driver != null) driver.Close();
            driver = null;
        }

        public void OpenAt(string url) => driver.Url = url;

        public void RefreshPage() => driver.Navigate().Refresh();

        internal IWebElement FindElement(By by)
        {
            return driver.FindElement(by);

        }
    }
}
