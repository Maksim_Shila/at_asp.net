﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalcForTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 9;
            Calculator calc = new Calculator();
            Console.WriteLine("Result: {0}", calc.Multiply(a, b));
            Console.ReadLine();
        }
    }
}
