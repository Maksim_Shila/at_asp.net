﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using WorkForAutomation.Models;
using WorkForAutomation.Services;

namespace WorkForAutomation.Test
{
     [TestFixture]
    class LetterTest : BaseTest
    {
         LoginService loginService = new LoginService();
         LetterService letterService = new LetterService();

         [Test]
         public void LetterTest_1_SendLetterAndFindItInOutBoxAndInBox()
         {
             loginService.LoginToMailBox(AccountFactory.Account);
             letterService.SendNewLetter(LetterFactory.Letter);
             Thread.Sleep(3000);
             letterService.CheckByConfirmSend();
             letterService.OpenInbox();
             letterService.OpenOutBox();
             bool letteFind = letterService.FindSendingLetter();
             Assert.AreEqual(letteFind, true);
         }
         
         [Test]
         public void LetterTest_2_TransferFindingLetterInTrashBox()
         {
             loginService.LoginToMailBox(AccountFactory.Account);
             letterService.FindSendingLetter();
             letterService.ClickCheckBox();
             letterService.DeleteButton();
             letterService.UpdatePage();
             bool letteFind = letterService.FindSendingLetter();
             Assert.AreEqual(letteFind, false);
         }

         [Test]
         public void LetterTest_3_DeleteLetterInTrashBox()
         {
             loginService.LoginToMailBox(AccountFactory.Account);
             letterService.OpenTrashBox();
             while (letterService.FindSendingLetter() != false) 
             {
                 letterService.FindSendingLetter();
                 letterService.ClickCheckBox();
                 letterService.DeleteButton();
                 letterService.UpdatePage();
             } 
             
             bool letteFind = letterService.FindSendingLetter();
             Assert.AreEqual(letteFind, false);
         }
    }
}
