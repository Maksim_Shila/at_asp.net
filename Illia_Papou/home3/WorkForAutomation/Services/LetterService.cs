﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using WorkForAutomation.Framework;
using WorkForAutomation.Models;
using WorkForAutomation.Pages;

namespace WorkForAutomation.Services
{
    public class LetterService
    {
        private LetterPage letterPage = new LetterPage();

        public void SendNewLetter(Letter letter)
        {
            letterPage.ButtonNewLetter.Click();
            letterPage.InputForEmailAddress.SendKeys(letter.Email);
            letterPage.InputForTitle.SendKeys(letter.Title);
            letterPage.InputForText.SendKeys(letter.InputForText);
            letterPage.ButtonForSendMail.Click();
            
        }

        public bool FindSendingLetter()
        {
            return letterPage.FindMailByTitle;
        }

        public void CheckByConfirmSend()
        {
            try
            {
                letterPage.ConfirmSendButton.Click();
            }
            catch (NoSuchElementException)
            {
                letterPage.FolderInbox.Click();
            }
                
        }

        public void OpenInbox()
        {
            letterPage.FolderInbox.Click();
        }

        public void OpenOutBox()
        {
            letterPage.FolderOutbox.Click();
            }

        public void OpenTrashBox()
        {
            letterPage.FolderOfDeletingFiles.Click();
        }

        public void DeleteButton()
        {
            letterPage.ButtonDelete.Click();
        }

        public void UpdatePage()
        {
            letterPage.ButtomForUpdatePage.Click();
        }
        

        public void ClickCheckBox()
        {
            letterPage.ClickCheckBoxByFindingMail.Click();
        }
    }
}
