﻿using WorkForAutomation.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkForAutomation.Models;

namespace WorkForAutomation.Pages
{
    class LetterPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By ButtonNewLetterCss = By.CssSelector("a[href='#compose']");
        private static readonly By InputForTextCss = By.CssSelector("div[role='textbox']");
        private static readonly By InputForTitleCss = By.CssSelector("div.mail-Compose-Field-Input input[type='text']");  
        private static readonly By InputForEmailAddressCss = By.CssSelector("div[name='to']");
        private static readonly By ButtonForSendMailCss = By.CssSelector("div.mail-Compose-From button");
        private static readonly By FolderInboxCss = By.CssSelector("a.ns-view-folder[href='#inbox']");
        private static readonly By FolderOutboxCss = By.CssSelector("a.ns-view-folder[href='#sent']");
        private static readonly By FindMailByTitleXPath = By.XPath("//span[@title='MyTest']//ancestor::a//label[@data-nb='checkbox']");
        private static readonly By ClickCheckBoxByFindingMailCss = By.CssSelector("span._nb-checkbox-flag._nb-checkbox-normal-flag");
        private static readonly By ButtonDeleteCss = By.CssSelector("div.ns-view-toolbar-button-delete[data-key*='delete']");
        private static readonly By ButtomForUpdatePageCss = By.CssSelector("div.ns-view-toolbar [data-click-action='mailbox.check']");
        private static readonly By FolderOfDeletingFilesCss = By.CssSelector("a.ns-view-folder[href='#trash']");
        private static readonly By ButtonConfirmSendCss = By.CssSelector("button[data-action='save']");

        public IWebElement ButtonNewLetter { get { return browser.FindElement(ButtonNewLetterCss); } }
        public IWebElement InputForText { get { return browser.FindElement(InputForTextCss); } }
        public IWebElement InputForTitle { get { return browser.FindElement(InputForTitleCss); } }
        public IWebElement InputForEmailAddress { get { return browser.FindElement(InputForEmailAddressCss); } }
        public IWebElement ButtonForSendMail { get { return browser.FindElement(ButtonForSendMailCss); } }
        public IWebElement FolderInbox { get { return browser.FindElement(FolderInboxCss); } }
        public IWebElement FolderOutbox { get { return browser.FindElement(FolderOutboxCss); } }
        public IWebElement ClickCheckBoxByFindingMail { get { return browser.FindElement(ClickCheckBoxByFindingMailCss); } }
        public IWebElement ButtonDelete { get { return browser.FindElement(ButtonDeleteCss); } }
        public IWebElement ButtomForUpdatePage { get { return browser.FindElement(ButtomForUpdatePageCss); } }
        public IWebElement FolderOfDeletingFiles { get { return browser.FindElement(FolderOfDeletingFilesCss); } }
        public IWebElement ConfirmSendButton { get { return browser.FindElement(ButtonConfirmSendCss); } }
        
        public bool FindMailByTitle
        {
            get
            {
                try
                {
                    return (browser.FindElement(FindMailByTitleXPath) != null ? true : false);
                }
                catch (NoSuchElementException)
                {

                    return false;
                }
            }

        }

        public bool FindElementOnPage
        {
            get { return (browser.FindElement(ButtonNewLetterCss) != null ? true : false); }
        }

        public bool IsSendConfirmed
        {
            get { return (browser.FindElement(ButtonConfirmSendCss) != null ? true : false); }
        }
    }
}
