﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkForAutomation.Models
{
    public class AccountFactory
    {
        private const string defaultMailUserLogin = "epam.testing";
        private const string defaultMailUserPassword = "epamtestingtask3";
        private const string defaultUserEmail = defaultMailUserLogin + "@yandex.com";

        private static readonly string WRONG_PASSWORD = defaultMailUserPassword + new Random().Next();
        private static readonly string NONEXISTENT_LOGIN = defaultMailUserLogin + new Random().Next();

        public static UserAccount Account
        {
            get { return new UserAccount(defaultMailUserLogin, defaultMailUserPassword, defaultUserEmail); }
        }

        public static UserAccount AccountWithWrongPassword
        {
            get { return new UserAccount(defaultMailUserLogin, WRONG_PASSWORD, defaultUserEmail); }
        }

        public static UserAccount NonExistedAccount
        {
            get { return new UserAccount(NONEXISTENT_LOGIN, defaultMailUserPassword, defaultUserEmail); }
        }
    }
}
