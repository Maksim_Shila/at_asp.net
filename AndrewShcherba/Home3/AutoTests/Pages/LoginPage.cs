﻿using OpenQA.Selenium;
using AutoTests.Utilities;

namespace AutoTests.Pages
{
    class LoginPage
    {
        public IWebElement LoginInput { get { return Browser.FindElement(By.CssSelector("input[name=login]")); } }
        public IWebElement PasswordInput { get { return Browser.FindElement(By.CssSelector("input[name=passwd]")); } }
        public IWebElement SubmitButton { get { return Browser.FindElement(By.CssSelector("form.auth button")); } }
    }

}

