﻿using AutoTests.Utilities;
using OpenQA.Selenium;

namespace AutoTests.Pages
{
    class InBoxMailPage
    {
        public IWebElement InboxButton { get { return Browser.FindElement(By.CssSelector(".ns-view-folders a[href='#inbox']")); } }

        public IWebElement FindLetter(string to, string subj)
        {
            return Browser.FindElement(By.XPath(string.Format(
              "//span[@title='{0}']/ancestor::div[@class='mail-MessageSnippet-Content']//span[@title='{1}']", to, subj)));
        }
    }
}
