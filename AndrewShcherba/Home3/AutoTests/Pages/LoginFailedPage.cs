﻿using OpenQA.Selenium;
using AutoTests.Utilities;

namespace AutoTests.Pages
{
    class LoginFailedPage
    {
        internal IWebElement ErrorMessage { get { return Browser.FindElement(By.CssSelector(".error-msg")); } }
    }
}
