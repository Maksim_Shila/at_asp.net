﻿using System;

namespace AutoTests.Models
{
    public class AccountFactory
    {
        private const string UserLogin = "andslobby";
        private const string UserPassword = "andrew030381";
        private const string UserEmail = UserLogin + "@yandex.ru";

        private static readonly string WrongUserLogin = +new Random().Next() + UserPassword;
        private static readonly string WrongUserPassword = new Random().Next() + UserLogin;

        public static UserAccount Account
        {
            get { return new UserAccount(UserLogin, UserPassword, UserEmail); }
        }

        public static UserAccount AccountWithWrongPassword
        {
            get { return new UserAccount(UserLogin, WrongUserPassword, UserEmail); }
        }

        public static UserAccount AccountWithWrongLogin
        {
            get { return new UserAccount(WrongUserLogin, UserPassword, UserEmail); }
        }
    }
}
