﻿using NUnit.Framework;
using AutoTests.Utilities;
using System.Threading;


namespace AutoTests.Tests
{
    class BaseTest
    {
        private const string baseUrl = "http://www.yandex.com";

        [SetUp]
        public void OpenBrowser()
        {
            Browser.Start();
            Thread.Sleep(3000); //shit happens
            Browser.Navigate(baseUrl);
        }

        [TearDown]
        public void closebrowser() => Browser.Quit();
    }
}
