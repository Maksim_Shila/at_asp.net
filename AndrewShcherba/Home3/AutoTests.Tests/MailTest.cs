﻿using NUnit.Framework;
using AutoTests.Models;
using AutoTests.Services;
using System;

namespace AutoTests.Tests
{
    [TestFixture]
    class MailTest : BaseTest
    {
        LoginService loginService = new LoginService();
        MailService mailService = new MailService();

        [Test]
        public void Mail_Test_Send_Check_In_Sent()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(AccountFactory.Account.Mail, "Hellow");
            Assert.That<String>(mailService.Subj, Is.EqualTo(mailService.CheckSentMail()));
        }

        [Test]
        public void Mail_Test_Send_Check_In_InBox()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(AccountFactory.Account.Mail, "Hellow");
            Assert.That<String>(mailService.Subj, Is.EqualTo(mailService.CheckInBoxMail()));
        }

        [Test]
        public void Mail_Test_Del_Letter_From_Sent()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(AccountFactory.Account.Mail, "Hellow");
            mailService.DelFormSentLetter();
            Assert.That<String>(mailService.Subj, Is.EqualTo(mailService.CheckDeletedMail()));
        }

        [Test]
        public void Mail_Test_Del_Letter_From_Trash()
        {

            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.SendMail(AccountFactory.Account.Mail, "Hellow");
            mailService.DelFormSentLetter();
            mailService.DelFormTrashLetter();
            Assert.That(mailService.CheckIsPermanentDeletedMail());

        }

    }
}
