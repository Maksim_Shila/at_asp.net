﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomation.Services.Models
{
	public static class FileFactory
	{
		private const string TEST_FILE_NAME = "test_file_";
		private const string TEST_FILE_EXT = ".txt";
		private const string TEST_FILE_CONTENTS = "Hello, I am a test file :)";

		public static FileInfo TestFile
		{
			get { return GenerateTestFile(); }
		}

		private static FileInfo GetTestFileInfo()
		{
			var path = AppDomain.CurrentDomain.BaseDirectory + 
				TEST_FILE_NAME +
				DateTime.Now.ToString("yyyy_dd_MM_HH_mm_ss") +
				TEST_FILE_EXT;
			return new FileInfo(path);
		}

		private static FileInfo GenerateTestFile()
		{
			var file = GetTestFileInfo();
			using (var sw = new StreamWriter(file.FullName))
			{
				sw.Write(TEST_FILE_CONTENTS);
			}
			return file;
		}

		public static void CleanUp()
		{
			var dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
			var files = dir.GetFiles(TEST_FILE_NAME + "*.*");
			foreach (var file in files)
			{
				File.Delete(file.FullName);
			}
		}
	}
}
