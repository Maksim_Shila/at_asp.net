﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomation.Services.Models
{
	public class Message
	{
		public string To { get; }
		public string Subject { get; }
		public string Body { get; }

		public Message(string to, string subject, string body)
		{
			To = to;
			Subject = subject;
			Body = body;
		}
	}
}
