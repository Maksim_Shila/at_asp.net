﻿using TestAutomation.Pages;
using TestAutomation.Services.Models;

namespace TestAutomation.Services
{
	public static class DiskLoginService
	{
		public static void Login(UserAccount account)
		{
			DiskLoginPage.LoginInput.SendKeys(account.Email);
			DiskLoginPage.PasswordInput.SendKeys(account.Password);
			DiskLoginPage.PasswordInput.Submit();
			//if (DiskMainPage.ContainsPopupCloseButton())
			//{
			//	DiskMainPage.PopupCloseButton.Click();
			//}
		}

		public static string RetrieveErrorOnFailedLogin()
		{
			return DiskLoginPage.ErrorMessage.Text;
		}

		public static string RetrieveUserNameOnLoginSuccess()
		{
			return DiskMainPage.UserName.Text;
		}
	}
}
