﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Services.Models;
using TestAutomation.Pages;

namespace TestAutomation.Services
{
	public static class MailLoginService
	{
		public static void Login(UserAccount account)
		{
			MailLoginPage.LoginInput.SendKeys(account.Email);
			MailLoginPage.PasswordInput.SendKeys(account.Password);
			MailLoginPage.SubmitButton.Click();
		}

		public static string RetrieveErrorOnFailedLogin()
		{
			return LoginFailedPage.ErrorMessage.Text;
		}

		public static string RetrieveUserNameOnLoginSuccess()
		{
			return MailMainPage.UserName.Text;
		}
		
	}
}
