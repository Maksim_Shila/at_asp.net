﻿using OpenQA.Selenium;
using TestAutomation.Framework;

namespace TestAutomation.Pages
{
	internal static class DiskMainPage
	{
		private static readonly By userName = By.CssSelector("span.header__username");
		private static readonly By uploadButton = By.CssSelector("input.button__attach");
		private static readonly By uploadCompleteDialogCloseButton = By.CssSelector("a.js-cross");
		private static readonly By uploadCompleteIcon = By.CssSelector(".b-item-upload__icon_done");
		private static readonly By replaceButton = By.CssSelector(".js-replace");
		private static readonly By deleteButton = By.CssSelector("[data-click-action='resource.delete']");
		private static readonly By trash = By.CssSelector("[href='/client/trash']");
		private static readonly By itemRemovedNotification = By.CssSelector(".notifications__item_moved div");
		private static readonly By popupCloseButton = By.CssSelector("._nb-popup-close");
		private static By GetFileNameSelector(string filename) => By.CssSelector($"[title='{filename}'] .nb-resource__text-clamped");

		internal static IWebElement UserName { get { return Browser.Instance.FindElement(userName); } }
		internal static IWebElement UploadButton { get { return Browser.Instance.FindElement(uploadButton); } }
		internal static IWebElement UploadCompleteDialogCloseButton { get { return Browser.Instance.FindElement(uploadCompleteDialogCloseButton); } }
		internal static IWebElement UploadCompleteIcon { get { return Browser.Instance.FindElement(uploadCompleteIcon); } }
		internal static IWebElement ReplaceButton { get { return Browser.Instance.FindElement(replaceButton); } }
		internal static IWebElement DeleteButton { get { return Browser.Instance.FindElement(deleteButton); } }
		internal static IWebElement Trash { get { return Browser.Instance.FindElement(trash); } }
		internal static IWebElement PopupCloseButton { get { return Browser.Instance.FindElement(popupCloseButton); } }

		internal static IWebElement GetFileName(string filename)
		{
			return Browser.Instance.FindElement(GetFileNameSelector(filename));
		}

		internal static bool ContainsFile(string filename)
		{
			return Browser.Instance.Contains(GetFileNameSelector(filename));
		}

		internal static bool ContainsReplaceButton()
		{
			return Browser.Instance.Contains(replaceButton);
		}

		internal static bool ContainsItemRemovedNotification()
		{
			return Browser.Instance.Contains(itemRemovedNotification);
		}

		internal static bool ContainsPopupCloseButton()
		{
			return Browser.Instance.Contains(popupCloseButton);
		}

	}
}
