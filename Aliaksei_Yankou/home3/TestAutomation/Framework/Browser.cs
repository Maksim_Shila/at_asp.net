﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomation.Framework
{
	public class Browser
	{
		private static readonly string chromeDriverPath = @"d:\code\EPAM\at_asp.net\Aliaksei_Yankou\home3\TestAutomation\bin\Debug\";
		private static readonly TimeSpan implicitlyWait = TimeSpan.FromSeconds(5);
		//private static readonly TimeSpan explicitlyWait = TimeSpan.FromSeconds(30);
		private static readonly TimeSpan pageLoadWait = TimeSpan.FromSeconds(5);

		private IWebDriver driver;

		private static Lazy<Browser> instanceHolder = new Lazy<Browser>(() => new Browser());

		public static Browser Instance
		{
			get { return instanceHolder.Value; }
		}

		private Browser() { }

		public Browser Start()
		{
			// It isn't good practice to init and setup driver here this way.
			// Better use driver factories, config files etc. But for our purposes we can leave it for now.
			driver = new ChromeDriver(chromeDriverPath);
			driver.Manage().Timeouts().ImplicitlyWait(implicitlyWait);
			driver.Manage().Timeouts().SetPageLoadTimeout(pageLoadWait);
			driver.Manage().Window.Maximize();
			return this;
		}

		public void Close()
		{
			if (driver != null) driver.Close();
			driver = null;
		}

		public void OpenAt(string url) => driver.Url = url;

		public void Refresh() => driver.Navigate().Refresh();

		internal IWebElement FindElement(By by) => driver.FindElement(by);


		//// Should not be used together with implicit wait
		//internal IWebElement FindElementClickable(By by)
		//{
		//	var wait = new WebDriverWait(driver, explicitlyWait);
		//	return wait.Until(ExpectedConditions.ElementToBeClickable(by));
		//}

		public bool Contains(By by)
		{
			return driver.FindElements(by).Count() > 0;
		}

	}
}
