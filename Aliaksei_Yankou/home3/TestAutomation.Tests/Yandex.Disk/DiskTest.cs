﻿using NUnit.Framework;
using TestAutomation.Services.Models;
using TestAutomation.Services;
using System.IO;
using TestAutomation.Framework;
using System.Threading.Tasks;
using System.Configuration;

namespace TestAutomation.Yandex.Disk.Tests
{
	[TestFixture]
	public class DiskTest : BaseYandexDiskTest
	{
		private readonly string expected_error_message_in_case_of_wrong_password = 
			ConfigurationManager.AppSettings["WrongUserNameOrPasswordErrorMessage"];
		
		[SetUp]
		public void Start()
		{
			DiskLoginService.Login(AccountFactory.ValidAccount);
		}

		[TearDown]
		public void Finish()
		{
			FileFactory.CleanUp();
		}

		[Test]
		public void UploadFileTest_UploadFile_TheFileIsOnYandexDisk()
		{
			var file = FileFactory.TestFile;
			DiskService.UploadFile(file);
			DiskService.CloseUploadCompleteDialog();
			DiskService.RefreshPage();
			Assert.That(DiskService.ContainsFile(file), Is.True);
		}

		[Test]
		public void DeleteFileTest_UploadAndDeleteFile_TheFileIsNotOnYandexDisk()
		{
			var file = FileFactory.TestFile;
			DiskService.UploadFile(file);
			DiskService.CloseUploadCompleteDialog();
			DiskService.RefreshPage();
			DiskService.SelectFile(file);
			DiskService.DeleteSelectedFiles();
			Assert.That(DiskService.ContainsFile(file), Is.False);
		}

		[Test]
		public void PermanentlyDeleteFileTest_UploadFileAndDeleteItFromTrash_TheFileIsNotOnYandexDisk()
		{
			var file = FileFactory.TestFile;
			DiskService.UploadFile(file);
			DiskService.CloseUploadCompleteDialog();
			DiskService.RefreshPage();
			DiskService.SelectFile(file);
			DiskService.DeleteSelectedFiles();
			DiskService.OpenTrash();
			DiskService.SelectFile(file);
			DiskService.DeleteSelectedFiles();
			Assert.That(DiskService.ContainsFile(file), Is.False);
		}
	}
}
