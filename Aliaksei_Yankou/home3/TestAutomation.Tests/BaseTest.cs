﻿using NUnit.Framework;
using TestAutomation.Framework;

namespace TestAutomation.Tests
{
	public class BaseTest
    {
		protected string baseUrl;

		[SetUp]
		public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);

		[TearDown]
		public void CloseBrowser() => Browser.Instance.Close();
    }
}
