﻿using NUnit.Framework;
using TestAutomation.Services.Models;
using TestAutomation.Services;
using System.Configuration;

namespace TestAutomation.Yandex.Mail.Tests
{
	[TestFixture]
	public class MailLoginTest : BaseYandexMailTest
	{
		private readonly string expected_error_message_in_case_of_account_not_exist = 
			ConfigurationManager.AppSettings["AccountNotExistentErrorMessage"];
		private readonly string expected_error_message_in_case_of_wrong_password = 
			ConfigurationManager.AppSettings["WrongPasswordErrorMessage"];
		[Test]
		public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
		{
			MailLoginService.Login(AccountFactory.NonExistedAccount);
			string actualErrorMessage = MailLoginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
		}

		[Test]
		public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
		{
			MailLoginService.Login(AccountFactory.AccountWithWrongPassword);
			string actualErrorMessage = MailLoginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
		}

		[Test]
		public void LoginTest_LoginWithValidAccount_UserEmailPresent()
		{
			MailLoginService.Login(AccountFactory.ValidAccount);
			string actualUserName = MailLoginService.RetrieveUserNameOnLoginSuccess();
			string expectedUserName = AccountFactory.ValidAccount.Email;
			Assert.That(expectedUserName, Is.EqualTo(actualUserName));
		}
	}
}
