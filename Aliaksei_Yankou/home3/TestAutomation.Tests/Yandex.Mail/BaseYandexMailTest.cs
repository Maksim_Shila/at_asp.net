﻿using System.Configuration;
using TestAutomation.Tests;

namespace TestAutomation.Yandex.Mail.Tests
{
	public class BaseYandexMailTest : BaseTest
	{
		public BaseYandexMailTest()
		{
			this.baseUrl = ConfigurationManager.AppSettings["YandexMailUrl"];
		}
	}
}
