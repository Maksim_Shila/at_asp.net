﻿using NUnit.Framework;
using System.Configuration;
using System.Threading.Tasks;
using TestAutomation.Services;
using TestAutomation.Services.Models;

namespace TestAutomation.Yandex.Mail.Tests
{
	[TestFixture]
	public class EmailTest : BaseYandexMailTest
	{
		private readonly string expected_error_message_in_case_of_invalid_recipient_email = 
			ConfigurationManager.AppSettings["InvalidEmailErrorMessage"];

		[SetUp]
		public void StartUp()
		{
			MailLoginService.Login(AccountFactory.ValidAccount);
		}

		[Test]
		public void SendingEmailTest_CreateAndSendEmail_MessageIsInSent()
		{
			var message = MessageFactory.ValidRecipientMessage;
			EmailService.SendEmail(message);
			EmailService.OpenSent();
			Assert.That(EmailService.ContainsMessage(message), Is.True);
		}

		[Test]
		public void SendingEmailTest_WrongRecipient_CorrectErrorMessage()
		{
			EmailService.SendEmail(MessageFactory.InvalidRecipientMessage);
			var actualError = EmailService.RetrieveInvalidEmailErrorMessage();
			Assert.That(actualError, Is.EqualTo(expected_error_message_in_case_of_invalid_recipient_email));
		}

		[Test]
		public void ReceivingEmailTest_CreateAndSendEmail_MessageIsInInbox()
		{
			var message = MessageFactory.ValidRecipientMessage;
			EmailService.SendEmail(message);
			EmailService.OpenInbox();
			Assert.That(EmailService.ConfirmMessageReceived(message), Is.True);
		}

		[Test]
		public void DeleteMessageTest_SendReceiveDeleteMessage_MessageIsNotInInbox()
		{
			var message = MessageFactory.ValidRecipientMessage;
			EmailService.SendEmail(message);
			EmailService.OpenInbox();
			EmailService.ConfirmMessageReceived(message);
			EmailService.SelectMessage(message);
			EmailService.DeleteSelectedMessages();
			EmailService.OpenInbox();
			Assert.That(EmailService.ContainsMessage(message), Is.False);
		}

		[Test]
		public void PermanentlyDeleteMessageTest_SendReceiveDeleteFromTrashMessage_MessageIsNotInTrash()
		{
			var message = MessageFactory.ValidRecipientMessage;
			EmailService.SendEmail(message);
			EmailService.OpenInbox();
			EmailService.ConfirmMessageReceived(message);
			// Both received and sent messages are deleted:
			EmailService.SelectMessage(message);
			EmailService.DeleteSelectedMessages();
			EmailService.OpenTrash();
			// Deleting received message:
			EmailService.SelectMessage(message);
			EmailService.DeleteSelectedMessages();
			// Deleting sent message:
			EmailService.SelectMessage(message);
			EmailService.DeleteSelectedMessages();
			Assert.That(EmailService.ContainsMessage(message), Is.False);
		}
	}
}
