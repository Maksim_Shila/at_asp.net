﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex.Framework
{
    public class Browser
    {
        private const string chromeDriverPath = @"F:\";
        private static readonly TimeSpan implicitlyWait = TimeSpan.FromSeconds(25);
        private static readonly TimeSpan pageLoadWait = TimeSpan.FromSeconds(25);

        private IWebDriver driver;

        private static Lazy<Browser> instanceHolder = new Lazy<Browser>(() => new Browser());

        public static Browser Instance
        {
            get { return instanceHolder.Value; }
        }

        private Browser() { }

        public Browser Start()
        {
            driver = new ChromeDriver(chromeDriverPath);
            driver.Manage().Timeouts().ImplicitlyWait(implicitlyWait);
            driver.Manage().Timeouts().SetPageLoadTimeout(pageLoadWait);
            driver.Manage().Window.Maximize();
            return this;
        }

        public void Close()
        {
            if (driver != null) driver.Close();
            driver = null;
        }

        public void OpenAt(string url) => driver.Url = url;

        internal IWebElement FindElement(By by) => driver.FindElement(by);
    }
}
