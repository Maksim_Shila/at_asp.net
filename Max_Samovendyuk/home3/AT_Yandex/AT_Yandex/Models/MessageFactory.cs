﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex.Models
{
    public class MessageFactory
    {
        private const string defaultRecepient = "Maxwebtest@yandex.by";
        private const string defaultMessageTheme = "Test theme";
        private const string defaultMessageText = "Test message";

        private static readonly string MESSAGE_THEME = defaultMessageTheme + new Random().Next();
        private static readonly string MESSAGE_TEXT = defaultMessageText + new Random().Next();

        public static MessageModel Message
        {
            get { return new MessageModel(defaultRecepient, MESSAGE_THEME, MESSAGE_TEXT); }
        }
    }
}
