﻿using AT_Yandex.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex.Pages
{
    public class MailBoxNewMessagePage
    {
        private Browser browser = Browser.Instance;

        private static readonly By MessageRecepientFieldByCss = By.CssSelector("div[name='to']");
        private static readonly By MessageSubjectFieldByCss = By.CssSelector("input[name='subj']");
        private static readonly By MessageTextAreaByCss = By.CssSelector("div[role='textbox']");
        private static readonly By SendButtonByCss = By.CssSelector("button[class*='js-send-button']");

        internal IWebElement MessageRecepientInput { get { return browser.FindElement(MessageRecepientFieldByCss); } }
        internal IWebElement MessageSubjectInput { get { return browser.FindElement(MessageSubjectFieldByCss); } }
        internal IWebElement MessageTextAreaInput { get { return browser.FindElement(MessageTextAreaByCss); } }
        internal IWebElement SendButton { get { return browser.FindElement(SendButtonByCss); } }
    }
}
