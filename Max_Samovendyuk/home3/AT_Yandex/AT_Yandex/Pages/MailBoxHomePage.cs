﻿using AT_Yandex.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex.Pages
{
    public class MailBoxHomePage
    {
        private Browser browser = Browser.Instance;

        private static readonly By InboxButtonByCss = By.CssSelector("div>[href='#inbox']");
        private static readonly By SentButtonByCss = By.CssSelector("div>[href='#sent']");
        private static readonly By TrashButtonByCss = By.CssSelector("div>[href='#trash']");
        private static readonly By ComposeButtonByCss = By.CssSelector("div.ns-view-container-desc>a[href*='#compose']");
        private static readonly By CheckMailButtonByCss = By.CssSelector("div[data-click-action='mailbox.check']");
        private static readonly By DeleteButtonByCss = By.CssSelector("div[class*='button-delete']");
        private static readonly By MessageCheckBoxByCss = By.CssSelector("label[data-nb='checkbox']");

        internal bool IsMessageExists(string messageSubject)
        {
            try
            {
                return browser.FindElement(By.XPath($"//span[@title='{messageSubject}']/ancestor::a")) != null ? true : false;
            }
            catch(NoSuchElementException)
            {
                return false;
            }
        }

        internal IWebElement MessageCheckBoxButton(string messageSubject)
        {
            return browser.FindElement(By.XPath($"//span[@title='{messageSubject}']/ancestor::a")).FindElement(MessageCheckBoxByCss);
        }

        internal IWebElement ComposeButton { get { return browser.FindElement(ComposeButtonByCss); } }
        internal IWebElement CheckMailButton { get { return browser.FindElement(CheckMailButtonByCss); } }
        internal IWebElement DeleteButton { get { return browser.FindElement(DeleteButtonByCss); } }
        internal IWebElement InboxButton { get { return browser.FindElement(InboxButtonByCss); } }
        internal IWebElement SentButton { get { return browser.FindElement(SentButtonByCss); } }
        internal IWebElement TrashButton { get { return browser.FindElement(TrashButtonByCss); } }
    }
}
