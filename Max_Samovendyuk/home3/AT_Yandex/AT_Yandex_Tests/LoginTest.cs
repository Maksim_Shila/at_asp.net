﻿using AT_Yandex.Models;
using AT_Yandex.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex_Tests
{
    [TestFixture]
    public class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();

        private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

        [Test]
        public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(UserAccountFactory.NonExistedAccount);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
        }

        [Test]
        public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(UserAccountFactory.AccountWithWrongPassword);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
        }
    }
}
