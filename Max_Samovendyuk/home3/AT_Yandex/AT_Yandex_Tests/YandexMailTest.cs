﻿using AT_Yandex.Models;
using AT_Yandex.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AT_Yandex_Tests
{
    [TestFixture]
    public class YandexMailTest : BaseTest
    {
        private LoginService loginService = new LoginService();
        private MessageService messageService = new MessageService();
        private MessageModel message;

        [SetUp]
        public void SetUp_LoginToAccount()
        {
            loginService.LoginToMailBox(UserAccountFactory.Account);
            message = MessageFactory.Message;
        }

        [Test]
        public void SendMessage_And_FindItInInbox()
        {
            messageService.SendNewMessage(message);
            Assert.That(messageService.IsMessageInInbox(message), Is.True);
        }

        [Test]
        public void SendMessage_And_FindItInSent()
        {
            messageService.SendNewMessage(message);
            Assert.That(messageService.IsMessageInSent(message), Is.True);
        }

        [Test]
        public void SendMessage_Then_DeleteIt_And_FindItInTrash()
        {
            messageService.SendNewMessage(message);
            messageService.DeleteMessageToTrash(message);
            Assert.That(messageService.IsMessageInTrash(message), Is.True);
        }

        [Test]
        public void SendMessage_Then_DeleteItToTrash_Then_DeleteItFromTrash()
        {
            messageService.SendNewMessage(message);
            messageService.DeleteMessageToTrash(message);
            messageService.DeleteMessageFromTrash(message);

            if (!messageService.IsMessageInTrash(message)) return;

            messageService.DeleteMessageFromTrash(message);
            
            Assert.That(messageService.IsMessageInTrash(message), Is.False);
        }
    }
}
