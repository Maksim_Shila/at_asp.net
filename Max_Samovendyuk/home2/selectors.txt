﻿На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска

XPATH:	$x("//input[@name='q']")
CSS:	$$("input[name='q']")

2. Кнопка "Поиск в Google"

XPATH:	$x("//input[@name='btnK']")
CSS:	$$("input[name='btnK']")

3. Кнопка "Мне повезет"

XPATH:	$x("//input[@name='btnI']")
CSS:	$$("input[name='btnI']")

На странице результатов гугла:
1. Ссылки на результаты поиска (все)

XPATH:	$x("//h3[@class='r']/a")
CSS:	$$("h3.r>a")

2. 5-я буква о в Goooooooooogle (внизу, где пагинация)

XPATH:	$x("//a[@aria-label='Page 5']/span")
CSS:	$$("[aria-label='Page 5']>span")

На странице yandex.com:
1. Login input field

XPATH:	$x("//input[@name='login']")
CSS:	$$("input[name='login']")

2. Password input field

XPATH:	$x("//input[@name='passwd']")
CSS:	$$("input[name='passwd']")

3. "Enter" button in login form

XPATH:	$x("//div[@class='domik2__submit']/button")
CSS:	$$(".domik2__submit>button")

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"

XPATH:	$x("//div[contains(@class,'ns-view-folders')]//a[@href='#inbox']")
CSS:	$$("div.ns-view-folders>a[href='#inbox']")

2. Ссылка "Исходящие"

XPATH:	$x("//div[contains(@class,'ns-view-folders')]//a[@href='#sent']")
CSS:	$$("div.ns-view-folders>a[href='#sent']")

3. Ссылка "Спам"

XPATH:	$x("//div[contains(@class,'ns-view-folders')]//a[@href='#spam']")
CSS:	$$("div.ns-view-folders>a[href='#spam']")

4. Ссылка "Удаленные"

XPATH:	$x("//div[contains(@class,'ns-view-folders')]//a[@href='#trash']")
CSS:	$$("div.ns-view-folders>a[href='#trash']")

5. Ссылка "Черновики"

XPATH:	$x("//div[contains(@class,'ns-view-folders')]//a[@href='#draft']")
CSS:	$$("div.ns-view-folders>a[href='#draft']")

6. Кнопка "Новое письмо"

XPATH:	$x("//div[@class='ns-view-container-desc']/a[@href='#compose']")
CSS:	$$("div.ns-view-container-desc>a[href*='#compose']")

7. Кнопка "Обновить"

XPATH:	$x("//div[@data-click-action='mailbox.check']")
CSS:	$$("div[data-click-action='mailbox.check']")

8. Кнопка "Отправить" (на странице нового письма)

XPATH:	$x("//button[contains(@class,'js-send')]")
CSS:	$$("button[class*='js-send']")

9. Кнопка "Пометить как спам"

XPATH:	$x("//div[contains(@class,'button-spam')]")
CSS:	$$("div[class*='button-spam']")

10. Кнопка "Пометить прочитанным"

XPATH:	$x("//div[contains(@class,'button-mark-as-read')]")
CSS:    $$("div[class*='button-mark-as-read']")

11. Кнопка "Переместить в другую директорию"

XPATH:	$x("//div[contains(@class,'button-folders-actions')]")
CSS:    $$("div[class*='button-folders-actions']")

12. Кнопка "Закрепить письмо"

XPATH:	$x("//div[contains(@class,'button-pin')]")
CSS:    $$("div[class*='button-pin']")

13. Селектор для поиска уникального письма

XPATH:	$x("//span[contains(@class,'Item_subject')]/span[@title]/ancestor::a")
CSS:	-

На странице яндекс диска
1. Кнопка загрузить файлы

XPATH:	$x("//input[@class='button__attach']")
CSS:    $$("input.button__attach")

2. Селектор для уникального файла на диске

XPATH:	
CSS:    

3. Кнопка скачать файл

XPATH:	$x("//button[@data-click-action='resource.download']")
CSS:    $$("button[data-click-action='resource.download']")

4. Кнопка удалить файл

XPATH:	$x("//button[@data-click-action='resource.delete']")
CSS:    $$("button[data-click-action='resource.delete']")

5. Кнопка в корзину

XPATH:	$x("//button[@data-click-action='resource.delete']")
CSS:    $$("button[data-click-action='resource.delete']")

6. Кнопка восстановить файл

XPATH:	$x("//button[@data-click-action='resource.restore']")
CSS:    $$("button[data-click-action='resource.restore']")