﻿using CalcTest;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcTest_Tests
{
    [TestFixture]
    public class CalcTest
    {
        private Calc calc;

        [SetUp]
        public void SetUp()
        {
            calc = new Calc();
        }

        [Test]
        public void SumTest_4plus6_return10()
        {
            //arrange
            int a = 4;
            int b = 6;
            int expected = 10;

            //act
            int actual = calc.Sum(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        } 

        [Test]
        public void SubstractTest_4minus3_returns1()
        {
            //arrange
            int a = 4;
            int b = 3;
            int expected = 1;

            //act
            int actual = calc.Substract(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DivideTest_5divideTo0_throwsArgumentException()
        {
            double a = 1;
            double b = 0;

            Assert.Throws<ArgumentException>(() => calc.Divide(a, b));
        }


        [Test]
        public void DivideTest_4divideTo2_returnsValueTypeOfDouble()
        {
            double a = 4.2;
            double b = 2;

            var result = calc.Divide(a, b);

            Assert.IsInstanceOf<double>(result);
        }

        [TestCase(2, 3, 6)]
        [TestCase(3, 2, 6)]
        [TestCase(3, 3, 9)]
        public void MultipleTest(int a, int b, int expected)
        {
            var actual = calc.Multiply(a, b);
            Assert.AreEqual(expected, actual);
        }

        // will be invoked after each test method
        [TearDown]
        public void TearDown()
        {
            calc = null;
        }
    }
}
