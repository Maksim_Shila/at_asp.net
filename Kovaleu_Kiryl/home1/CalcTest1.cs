﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConsoleApplication1.Tests
{
    [TestClass]
    public class CalcTest1
    {
        [TestMethod]
        public void Sum_5Plus3_7Returned()
        {
            Calc calc = new Calc();

            int actual = calc.sum(5, 3);

            Assert.AreEqual(8, actual);
        }

        [TestMethod]
        public void substract_5Minus3_2Returned()
        {
            Calc calc = new Calc();

            int result = calc.substract(5, 3);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void multiplication_5multiplication3_15Returned()
        {
            Calc calc = new Calc();

            int result = calc.multiplication(5, 3);

            Assert.AreEqual(15, result);
        }

        [TestMethod]
        public void division_6division3_2Returned()
        {
            Calc calc = new Calc();

            int result = calc.division(6, 3);

            Assert.AreEqual(2, result);
        }
    }
}
