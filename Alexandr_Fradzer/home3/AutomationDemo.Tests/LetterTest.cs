﻿using AutomationDemo.Models;
using AutomationDemo.Services;
using AutomationDemo.Services.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class LetterTest : BaseTest
    {
        LoginService loginService = new LoginService();
        LetterService letterService = new LetterService();


        //Send letter and check in inbox
        [Test]
        public void LetterTest_SendLetterTest_ContainletterInInbox()
        {
            bool expected = true;
            loginService.LoginToMailBox(AccountFactory.Account);
            letterService.PushWriteLetterButton();
            letterService.SendLetter(LetterFactory.Letter());
            letterService.PushInboxButton();
            letterService.RefreshPage();
            bool actual = letterService.haveLetterByText();

            Assert.AreEqual(expected, actual);
        }


        //Send letter and check in outbox
        [Test]
        public void LetterTest_SendLetterTest_ContainletterInOutbox()
        {
            bool expected = true;
            loginService.LoginToMailBox(AccountFactory.Account);
            letterService.PushWriteLetterButton();
            letterService.SendLetter(LetterFactory.Letter());
            letterService.PushOutboxButton();
            letterService.RefreshPage();

            bool actual = letterService.haveLetterByText();

            Assert.AreEqual(expected, actual);
        }

        //Send letter, delete letter and check

        [Test]
        public void LetterTest_DeleteLetterTest_NotContainletter()
        {
            bool expected = false;
            loginService.LoginToMailBox(AccountFactory.Account);

            letterService.PushWriteLetterButton();
            letterService.SendLetter(LetterFactory.Letter());
            letterService.PushInboxButton();

            letterService.SelectLetterByText();
            letterService.PushDeleteButton();
            letterService.RefreshPage();

            bool actual = letterService.haveLetterByText();

            Assert.AreEqual(expected, actual);
        }


        //send letter and full delete and check in deleted
        [Test]
        public void LetterTest_FullDeleteLetterTest_NotContainletterInDeleted()
        {
            bool expected = false;
            loginService.LoginToMailBox(AccountFactory.Account);

            letterService.PushWriteLetterButton();
            letterService.SendLetter(LetterFactory.Letter());
            letterService.PushInboxButton();
            letterService.RefreshPage();

            letterService.SelectLetterByText();
            letterService.PushDeleteButton();
            letterService.RefreshPage();

            letterService.PushDeletedLetters();

            letterService.SelectLetterByText();
            letterService.PushDeleteButton();
            letterService.RefreshPage();

            bool actual = letterService.haveLetterByText();

            Assert.AreEqual(expected, actual);
        }
    }
}
