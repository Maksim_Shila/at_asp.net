﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Models
{
    public class Letter
    {
        public string To { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public Letter(string to, string theme, string text)
        {
            To = to;
            Theme = theme;
            Text = text;
        }
    }
}
