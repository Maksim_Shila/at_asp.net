﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Models
{
    public static class LetterFactory
    {
        private const string defaultLetterTo = "kyklavot@yandex.ru";
        private const string defaultLetterTheme = "Theme for test";
        private const string defaultLetterText ="Message for test";

        public static Letter Letter()
        {
            return new Letter(defaultLetterTo, defaultLetterTheme, defaultLetterText);
        }

        public static Letter LetterForRandomMail()
        {
            return new Letter(defaultLetterTo + new Random().Next(), defaultLetterTheme, defaultLetterText);
        }
    }
}
