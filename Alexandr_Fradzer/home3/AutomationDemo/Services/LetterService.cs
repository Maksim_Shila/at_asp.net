﻿using AutomationDemo.Framework;
using AutomationDemo.Models;
using AutomationDemo.Pages;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Services
{
    public class LetterService
    {
        private MailPage mailPage = new MailPage();

        public void PushWriteLetterButton()
        {
            mailPage.WriteLetterSubmit.Click();
        }

        public void SendLetter(Letter letter)
        {
            Browser browser = Browser.Instance;
            mailPage.ToInput.SendKeys(letter.To);
            mailPage.ThemeInput.SendKeys(letter.Theme);
            new Actions(browser.Driver).SendKeys(mailPage.TextInput, letter.Text).Perform();
            mailPage.SendLetterSubmit.Click();
        }

        public void PushInboxButton()
        {
            mailPage.InboxButton.Click();
            if(mailPage.HaveConfirmButton)
            {
                mailPage.ConfirmButton.Click();
                mailPage.InboxButton.Click();
            }
        }

        public void PushOutboxButton()
        {
            mailPage.OutboxButton.Click();
            if (mailPage.HaveConfirmButton)
            {
                mailPage.ConfirmButton.Click();
                mailPage.OutboxButton.Click();
            }
           
        }

        public void SelectLetterByText()
        {
            mailPage.LabelLetterButton.Click();
        }

        public void PushDeletedLetters()
        {

            mailPage.DeletedLettersSubmit.Click();
        }

        public bool haveLetterByText()
        {
            return mailPage.HaveLetterByText;
        }

        public void PushDeleteButton()
        {
            mailPage.DeleteSubmit.Click();
        }

        public void RefreshPage()
        {
            Browser browser = Browser.Instance;
            browser.Driver.Navigate().Refresh();
        }

    }
}
