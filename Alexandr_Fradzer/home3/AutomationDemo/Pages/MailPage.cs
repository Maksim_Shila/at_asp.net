﻿using AutomationDemo.Framework;
using AutomationDemo.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class MailPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By userBlockByCss = By.CssSelector(".ns-view-head-user .mail-User-Name");
        private static readonly By writeLetterSubmitByCss = By.CssSelector("div.ns-view-container-desc a[href='#compose']");
        private static readonly By inputToByCss = By.CssSelector("div.ns-view-compose-field-to input[type='text']");
        private static readonly By inputThemeByCss = By.CssSelector("label[data-key='view=compose-field-subject'] input.mail-Compose-Field-Input-Controller");
        private static readonly By inputTextByCss = By.CssSelector("div[role='textbox']>div");
        private static readonly By sendLetterByCss = By.CssSelector("div.mail-Compose-Field-Actions_left button");
        private static readonly By inboxButtonByCss = By.CssSelector("div[data-key='view=folders'] a[href='#inbox']");
        private static readonly By outboxButtonByCss = By.CssSelector("div[data-key='view=folders'] a[href='#sent']");
        private static readonly By checkboxLetterByXpath = By.XPath("//span[@title='Message for test']//ancestor::a//label[@data-nb='checkbox']");
        private static readonly By confirmSendLetterButtonByCss = By.CssSelector("button[data-action='save']");
        private static readonly By deleteButtonByCss = By.CssSelector("div.ns-view-toolbar-button-delete");
        private static readonly By deletedLettersButtonByCss = By.CssSelector("div[data-key='view=folders'] a[href='#trash']");


        internal bool HaveUserBlockElement {
            get {
                try
                {
                    return browser.FindElement(userBlockByCss) != null ? true: false;                    
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
        }

        internal IWebElement WriteLetterSubmit { get { return browser.FindElement(writeLetterSubmitByCss); } }
        internal IWebElement DeletedLettersSubmit { get { return browser.FindElement(deletedLettersButtonByCss); } }

        internal IWebElement DeleteSubmit { get { return browser.FindElement(deleteButtonByCss); } }

        internal IWebElement SendLetterSubmit { get { return browser.FindElement(sendLetterByCss); } }

        internal IWebElement ToInput { get { return browser.FindElement(inputToByCss); } }

        internal IWebElement ThemeInput { get { return browser.FindElement(inputThemeByCss); } }

        internal IWebElement TextInput { get { return browser.FindElement(inputTextByCss); } }

        internal IWebElement InboxButton { get { return browser.FindElement(inboxButtonByCss); } }

        internal IWebElement LabelLetterButton { get { return browser.FindElement(checkboxLetterByXpath); } }


        internal bool HaveLetterByText
        {
            get
            {
                try
                {
                    return browser.FindElement(checkboxLetterByXpath) != null ? true : false;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
        }
        internal bool HaveConfirmButton
        {
            get
            {
                try
                {
                    return browser.FindElement(confirmSendLetterButtonByCss) != null ? true : false;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
        }

        internal IWebElement ConfirmButton { get { return browser.FindElement(confirmSendLetterButtonByCss); } }

        internal IWebElement OutboxButton { get { return browser.FindElement(outboxButtonByCss); } }
    }
}
