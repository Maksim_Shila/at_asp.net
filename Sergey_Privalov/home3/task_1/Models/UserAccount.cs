﻿namespace task_1.Models
{
    public class UserAccount
    {
        public string Login { get; }
        public string Password { get; }
        public string Email { get; set; }

        public UserAccount(string name, string password, string email)
        {
            Login = name;
            Password = password;
            Email = email;
        }
    }
}
