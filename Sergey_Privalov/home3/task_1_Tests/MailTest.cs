﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using task_1.Models;
using task_1.Models.Factory;
using task_1.Service;

namespace task_1_Tests
{
    [TestFixture]
    class MailTest : BaseTest
    {
        LoginService mailLoginService = new LoginService();
        MailService mailService = new MailService();

        private const string expected_error_message_in_case_of_wrong_send_to_mail = "Invalid email address.";

        [SetUp]
        public void SetUp()
        {
            mailLoginService.LoginToMailBox(AccountFactory.Account);
        }

        [Test]
        public void MailTest_PositiveSendMail()
        {
            NewMail newMail = NewMailFactory.CorrectMail;
            mailService.SendMail(newMail);
            bool isSuccessSend = this.mailService.Check_Inbox_Sent_New_Mail();
            Assert.That(true, Is.EqualTo(isSuccessSend));
        }

        [Test]
        public void MailTest_Retrive_Error_With_NewMail_With_WrongSendToMail()
        {
            mailService.SendMailWithWrongMail(NewMailFactory.UncorrectMail);
            string error_invalid_send_to_mail = mailService.RetrieveErrorOnFailedSendMail();
            Assert.That(expected_error_message_in_case_of_wrong_send_to_mail,Is.EqualTo(error_invalid_send_to_mail));
        }

        [Test]
        public void MailTest_DeleteEMailMessageFromInbox()
        {
            int countEMailMessagesBeforeDelete = mailService.Get_Count_Mails();
            mailService.Delete_EmailMessage_And_Checkmail();
            int countEmailMessagesAfterDelete = mailService.Get_Count_Mails();
            Assert.That(countEmailMessagesAfterDelete, Is.LessThan(countEMailMessagesBeforeDelete));
        }

        [Test]
        public void MailTest_DeleteEMailMessageFromTrash()
        {
            mailService.Move_On_Page_Trash();
            Task.Delay(TimeSpan.FromSeconds(2)).Wait();
            int countEMailMessagesBeforeDelete = mailService.Get_Count_Mails();
            mailService.Delete_EmailMessage_And_Checkmail();
            int countEmailMessagesAfterDelete = mailService.Get_Count_Mails();
            Assert.That(countEmailMessagesAfterDelete, Is.LessThan(countEMailMessagesBeforeDelete));
        }
    }
}
