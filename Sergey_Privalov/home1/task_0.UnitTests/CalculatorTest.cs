﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using task_0;

namespace task_0.UnitTests
{
    [TestFixture]
    public class CalculatorTest
    {
        private Calculator _calc = new Calculator();

        [Test]
        [TestCase(2,3)]
        public void Sum2_3_5(int a, int b)
        {
            double result = _calc.Sum(a, b);
            Assert.AreEqual(result,5);
        }

        [Test]
        [TestCase(4, 2)]
        public void Division4_2_2(int a, int b)
        {
            double result = this._calc.Division(a, b);
            Assert.AreEqual(result, 2);
        }

        [Test]
        [TestCase(2, 0)]
        public void Division2_0_exc(int a, int b)
        {
            double result = this._calc.Division(a, b);
            Assert.AreEqual(result, 0);
        }

        [Test]
        [TestCase(2, 3)]
        public void Multiplication2_3_5(int a, int b)
        {
            double result = this._calc.Multiplication(a, b);
            Assert.AreEqual(result, 6);
        }

        [Test]
        [TestCase(2, 3)]
        public void Subtraction2_3_5(int a, int b)
        {
            double result = this._calc.Subtraction(a, b);
            Assert.AreEqual(result, -1);
        }
    }
}
