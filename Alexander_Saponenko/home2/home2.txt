�� �������� google.com �������� ��������� CSS � XPATH ���������:
1. ������ ��� ����� �������� ��� ������	
CSS: #lst-ib
XPATH: //input[@id='lst-ib']
2. ������ "����� � Google"
CSS: [name=btnK]
XPATH: //input[@name='btnK']
3. ������ "��� �������"
CSS: [name=btnI]
XPATH: //input[@name='btnI']

�� �������� ����������� �����:
1. ������ �� ���������� ������ (���)
css: .r>a
XPATH: //*[@class='r']/a
2. 5-� ����� � � Goooooooooogle (�����, ��� ���������)
css: #nav :nth-child(6) span
xPATH: /*[@id='nav']//td[6]//span

�� �������� yandex.com:
1. Login input field
css: [name=login]
XPATH: //input[@name='login']
2. Password input field
CSS: [name=passwd]
XPATH: //input[@name='passwd']
3. "Enter" button in login form
CSS: .domik2__submit button
XPATH: //*[@class='domik2__submit']/button

�� �������� ������ ����� (� ���� ��� ����� - ���������������)
1. ������ "��������"
css: [data-fid='1']
XPATH: //*[@data-fid='1']
2. ������ "���������"
css: [data-fid='4']
XPATH: //*[@data-fid='4']
3. ������ "����"
css: [data-fid='2']
XPATH: //*[@data-fid='2']
4. ������ "���������"
css: [data-fid='3']
XPATH: //*[@data-fid='3']
5. ������ "���������"
css: [data-fid='6']
XPATH: //*[@data-fid='6']
6. ������ "����� ������"
CSS: a[href='#compose']
XPATH: //a[@href='#compose']
7. ������ "��������"
CSS: .ns-view-toolbar [data-click-action='mailbox.check']
XPATH: //*[contains(@class,'ns-view-toolbar')]//*[@data-click-action='mailbox.check']
8. ������ "���������" (�� �������� ������ ������)
CSS: #nb-45
XPATH: //*[@id='nb-45']
9. ������ "�������� ��� ����"
CSS: .ns-view-toolbar-button-spam
XPATH: //*[contains(@class,'ns-view-toolbar-button-spam')]
10. ������ "�������� �����������"
CSS: .ns-view-toolbar-button-mark-as-read
XPATH: //*[contains(@class,'ns-view-toolbar-button-mark-as-read')]
11. ������ "����������� � ������ ����������"
CSS: .ns-view-toolbar-button-folders-actions
XPATH: //*[contains(@class,'ns-view-toolbar-button-folders-actions')]
12. ������ "��������� ������"
CSS: .ns-view-toolbar-button-pin
XPATH: //*[contains(@class,'ns-view-toolbar-button-pin')]
13. �������� ��� ������ ����������� ������
CSS: ._nb-input-content input
XPATH: //*[@class='_nb-input-content']/input

�� �������� ������ �����
1. ������ ��������� �����
CSS: .button__attach
XPATH: //*[@class='button__attach']
2. �������� ��� ����������� ����� �� �����
CSS: [data-id='/disk/File_Name']
XPATH: //*[@data-id='/disk/File_Name']
3. ������ ������� ����
CSS: button[data-click-action='resource.download']
XPATH: //button[@data-click-action='resource.download']
4. ������ ������� ����
CSS: button[data-click-action='resource.delete']
XPATH: //button[@data-click-action='resource.delete']
5. ������ � �������
CSS: button[data-click-action='resource.delete']
XPATH: //button[@data-click-action='resource.delete']
6. ������ ������������ ����
CSS: button[data-click-action='resource.restore']
XPATH: //button[@data-click-action='resource.restore']