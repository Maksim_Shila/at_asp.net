﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;
using AutomationDemo.Services;

namespace AutomationDemo.Pages
{
    class LoginSuccessPage
    {
        Browser browser = Browser.Instance;
        static readonly By successLoginByCss = By.CssSelector(".mail-User-Name");
        static readonly By newMessageButtonByCss = By.CssSelector("a[href='#compose']");
        static readonly By destinationByCss = By.CssSelector("div[name='to']");
        static readonly By messageBoxByCss = By.CssSelector("div[role='textbox']");
        static readonly By sendButtonByCss = By.CssSelector(".mail-Compose-From button");
        static readonly By inboxByCss = By.CssSelector("div > [href='#inbox']");
        static readonly By UniqueLetterByXPath = By.XPath("//span[@title='" + (new OptionMessageService().Message) + "']/ancestor::a");
        static readonly By sentByCss = By.CssSelector("div > [href='#sent']");
        static readonly By trashByCss = By.CssSelector("div > [href='#trash']");


        internal IWebElement SuccessLogin { get { return browser.FindElement(successLoginByCss); } }

        internal IWebElement NewMessageButton { get { return browser.FindElement(newMessageButtonByCss); } }
        internal IWebElement Destination { get { return browser.FindElement(destinationByCss); } }

        internal IWebElement MessageBox { get { return browser.FindElement(messageBoxByCss); } }
        internal IWebElement SendButton { get { return browser.FindElement(sendButtonByCss); } }
        internal IWebElement InboxLink { get { return browser.FindElement(inboxByCss); } }
        internal IWebElement UniqueLetter { get { return browser.FindElement(UniqueLetterByXPath); } }
        internal IWebElement SentLink { get { return browser.FindElement(sentByCss); } }
        internal IWebElement TrashLink { get { return browser.FindElement(trashByCss); } }

    }
}
