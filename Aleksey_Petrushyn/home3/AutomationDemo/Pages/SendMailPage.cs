﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;

namespace AutomationDemo.Pages
{
	class SendMailPage
	{
		Browser browser = Browser.Instance;

		private static readonly By sendToByCss = By.CssSelector("div[class='mail-Compose-Field-Input'] div[name='to']");    //ищим поле ввода адреса получателя
		private static readonly By sendSubjectByCss = By.CssSelector("label[data-key$='subject'] input[type=text]");        //ищим поле ввода темы письма
		private static readonly By sendMessageByCss = By.CssSelector("div[role='textbox']");                                //ищим поле ввода текста письма
		private static readonly By sendButtonByCss = By.CssSelector("div[class$=From] button");                             //ищим кнопку отправить
		private static readonly By goToOutboxByCss = By.CssSelector("a[href='#sent']");                                     //переходим в папку Отправленные

		private static readonly By searchLastLetterByXPath =
			By.XPath(
				"//div[@class='ns-view-container-desc mail-MessagesList js-messages-list']/div[1]//" +
				"span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span");																//ищим тему последнего отправленного письма
		public IWebElement SendToInput { get { return browser.FindElement(sendToByCss); } }
		public IWebElement SubjectInput { get { return browser.FindElement(sendSubjectByCss); } }
		public IWebElement SendMessageDiv { get { return browser.FindElement(sendMessageByCss); } }
		public IWebElement SendButton { get { return browser.FindElement(sendButtonByCss); } }
		public IWebElement GoToOutbox { get { return browser.FindElement(goToOutboxByCss); } }
		public IWebElement LastLetter { get { return browser.FindElement(searchLastLetterByXPath); } }

	}
}
