﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AutomationDemo.Pages
{
	public class SendMailToTrashPage
	{
		Browser browser = Browser.Instance;

		private static readonly By checkLastLetterByXPath = By.XPath("//div[@class='ns-view-container-desc mail-MessagesList js-messages-list']" +
																																 "/div[1]//label[@data-nb='checkbox']/span[1]");      //ставим галочку на последнем письме
		private static readonly By clickDeleteByCss = By.CssSelector("[class$=-delete]");       //нижимаем "удалить"
		private static readonly By goToTrashByCss = By.CssSelector("[href='#trash']");          //переходим в корзину

		public IWebElement CheckLastLetter { get { return browser.FindElement(checkLastLetterByXPath); } }
		public IWebElement ClickDelete { get { return browser.FindElement(clickDeleteByCss); } }
		public IWebElement GoToTrash { get { return browser.FindElement(goToTrashByCss); } }

	}
}
