﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutomationDemo.Pages;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Services
{
	public class SendMailService
	{
		LoginService loginService = new LoginService();
		LoginSuccessfulPage page = new LoginSuccessfulPage();
		SendMailPage sendMailPage = new SendMailPage();
		SendMailToTrashPage sendMailToTrashPage = new SendMailToTrashPage();
		public void SendMailBox(UserAccount account, string email, string subject, string message)
		{
			loginService.LoginToMailBox(account);             //заходим в свой аккаунт
			page.WriteButton.Click();                         //находим и нажимаем кнопку "Написать"
			sendMailPage.SendToInput.SendKeys(email);         //вводим адрес получателя
			sendMailPage.SubjectInput.SendKeys(subject);      //вводим тему письма
			sendMailPage.SendMessageDiv.SendKeys(message);    //вводим текст письма
			sendMailPage.SendButton.Click();                  //нажимаем кнопку "Отправить"
			Thread.Sleep(1000);           //задержка
			sendMailPage.GoToOutbox.Click();                  //нажимам на ссылку "Отправленные"
			Thread.Sleep(1000);           //задержка
		}

		public bool SearchMail(string subject)
		{
			string lastLetterSubject = LastLetterSubject();   //сравниваем тему последнего отправленного письма с заданной
			if (lastLetterSubject == subject)
				return true;
			return false;
		}

		public void LoginAndGoToOutbox(UserAccount account)
		{
			loginService.LoginToMailBox(account);
			sendMailPage.GoToOutbox.Click();
			Thread.Sleep(500);           //задержка
		}

		public string LastLetterSubject()					//считываем текст поля Subject
		{
			return sendMailPage.LastLetter.Text;
		}
		//входим //переходим в отправленные //выбираем флажок напротив последнего письма //запоминаем его тему //нажимаем в корзину //переходим в карзину //находим
		public void DeleteAndGoToTrashBox()
		{
			Thread.Sleep(500);           //задержка
			sendMailToTrashPage.CheckLastLetter.Click();    //ставим галочку на последнем письме
			Thread.Sleep(500);           //задержка
			sendMailToTrashPage.ClickDelete.Click();        //нажимаем УДАЛИТЬ
			Thread.Sleep(500);           //задержка
			sendMailToTrashPage.GoToTrash.Click();          //переходим в корзину
			Thread.Sleep(500);           //задержка
		}

		public void LoginAndGoToTrash(UserAccount account)
		{
			loginService.LoginToMailBox(account);
			Thread.Sleep(500);           //задержка
			sendMailToTrashPage.GoToTrash.Click();      //Переходим в корзину
			Thread.Sleep(500);           //задержка
		}

		public void DeleteFromTrashBox()
		{
			sendMailToTrashPage.CheckLastLetter.Click();    //ставим галочку на последнем письме
			Thread.Sleep(500);           //задержка
			sendMailToTrashPage.ClickDelete.Click();        //нажимаем УДАЛИТЬ
			Thread.Sleep(500);           //задержка
		}

		public bool IsLetterExist()           //ищим есть ли хотя бы одно письмо 
		{
			Thread.Sleep(500);									//задержка
			return sendMailPage.LastLetter.Displayed;
		}
	}
}
