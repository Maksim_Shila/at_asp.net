﻿using NUnit.Framework;
using AutomationDemo.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Tests
{
	class BaseTest
	{
		private const string baseUrl = "http://www.yandex.by";

		[SetUp]
		public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);

		[TearDown]
		public void CloseBrowser() => Browser.Instance.Close();
	}
}
