﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
	[TestFixture]
	class SendMailTest : BaseTest
	{
		SendMailService sendMailService = new SendMailService();

		private const string email = "aliakseypiatrushyn@gmail.com";
		private string subject = "Test_" + new Random().Next();
		private const string messageText = "Test message from Yandex mail";
		[Test]
		public void SendMailTest_SendMessageAndCheck()
		{

			sendMailService.SendMailBox(AccountFactory.Account, email, subject, messageText);
			bool isDisplayedRightSubjects = sendMailService.SearchMail(subject);
			Assert.That(isDisplayedRightSubjects);
		}

		[Test]
		public void SendMailTest_SendMailToTrash()
		{
			sendMailService.LoginAndGoToOutbox(AccountFactory.Account);
			string delMailSubject = sendMailService.LastLetterSubject();
			sendMailService.DeleteAndGoToTrashBox();
			bool isDisplayedRightSubjects = sendMailService.SearchMail(delMailSubject);
			Assert.That(isDisplayedRightSubjects);
		}

		[Test]
		public void SendMailTest_DeleteFromTrash()
		{
			sendMailService.LoginAndGoToTrash(AccountFactory.Account);
			string delMailSubject = sendMailService.LastLetterSubject();
			sendMailService.DeleteFromTrashBox();
			bool isDisplayedRight = true;
			if (sendMailService.IsLetterExist())          //если письма в корзине есть - проверяем удалилось ли нужное письмо
				isDisplayedRight = !sendMailService.SearchMail(delMailSubject);

			Assert.That(isDisplayedRight);
		}
	}
}
