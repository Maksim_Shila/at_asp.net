﻿using NUnit.Framework;
using AutomationTask3.Services;
using AutomationTask3.Models;
using AutomationTask3.Services.Models;
using System.Threading;

namespace AutomationTask3.Tests
{
    [TestFixture]
    class MailTest : BaseTest
    {
        LoginService loginService = new LoginService();
        MailService mailService = new MailService();
        Mail mail = MailFactory.RandomMail;
    
        [Test]
        public void MailTest_SendingMail_MailInBoxIsPresent()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.CreateNewMail();
            mailService.WriteMail(mail);
            mailService.SendMail();
            //waiting for full mail sending
            Thread.Sleep(3000);
            Assert.That(mailService.FindMailInReceivedFolder(mail), Is.EqualTo(true));
            Assert.That(mailService.FindMailInTransmitedFolder(mail), Is.EqualTo(true));
        }
        [Test]
        public void MailTest_RemovingMail_RemovedMailInTrashIsPresent()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.CreateNewMail();
            mailService.WriteMail(mail);
            mailService.SendMail();
            //waiting for full mail sending
            Thread.Sleep(3000);
            mailService.DeleteMail(mail);
            Assert.That(mailService.FindMailInTrashFolder(mail), Is.EqualTo(true));
        }
        [Test]
        public void MailTest_RemovingMail_RemovedMailInTrashIsRemoved()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailService.CreateNewMail();
            mailService.WriteMail(mail);
            mailService.SendMail();
            //waiting for full mail sending
            Thread.Sleep(3000);
            mailService.FullDeleteMail(mail);
            Assert.That(mailService.FindMailInTrashFolder(mail), Is.EqualTo(false));
        }
    }
}
