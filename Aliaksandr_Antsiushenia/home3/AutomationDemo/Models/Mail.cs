﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTask3.Models
{
    public class Mail
    {
        //title of mail
        public string Title { get; set; }
        //receiver of mail
        public string Receiver { get; set; }
        //text of mail
        public string Text { get; set; }
        public Mail(string receiver, string title, string text)
        {
            Title = title;
            Receiver = receiver;
            Text = text;
        }
    }
}
