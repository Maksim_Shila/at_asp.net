﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTask3.Models;
using AutomationTask3.Pages;

namespace AutomationTask3.Services
{
    public class MailService
    {
        private MailPage mailPage = new MailPage();

        //enter to creating new mail
        public void CreateNewMail()
        {
            mailPage.WriteMailButton.Click();
        }

        //fill all text areas
        public void WriteMail(Mail mail)
        {
            if (mailPage.SendMailButton != null)
            {
                mailPage.ReceiverInput.SendKeys(mail.Receiver);
                mailPage.TitleInput.SendKeys(mail.Title);
                mailPage.MailTextInput.SendKeys(mail.Text);
            }   
        }
        public void SendMail()
        {
            mailPage.SendMailButton.Click();
        }
        public bool FindMailInReceivedFolder(Mail mail)
        {
            mailPage.ReceivedMailButton.Click();
            if (mailPage.FindMail(mail.Title) == null)
                return false;
            else
                return true;
        }
        public bool FindMailInTransmitedFolder(Mail mail)
        {
            mailPage.TransmitedMailButton.Click();
            if (mailPage.FindMail(mail.Title) == null)
                return false;
            else
                return true;
        }
        public bool FindMailInTrashFolder(Mail mail)
        {
            mailPage.RemovedMailButton.Click();
            if (mailPage.FindMail(mail.Title) == null)
                return false;
            else
                return true;
        }
        public void DeleteMail(Mail mail)
        {
            mailPage.CheckNewMailButton.Click();
            mailPage.ReceivedMailButton.Click();
            mailPage.FindMail(mail.Title).Click();
            mailPage.DeleteMailButton.Click();
        }
        public void FullDeleteMail(Mail mail)
        {
            DeleteMail(mail);
            mailPage.RemovedMailButton.Click();
            mailPage.FindMail(mail.Title).Click();
            mailPage.DeleteMailButton.Click();
        }
    }
}
