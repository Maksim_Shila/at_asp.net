﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_TASK1
{
    class Something
    {
        int a, b, c;
        public Something()
        {
            a = 0;
            b = 0;
            c = 0;
        }
        public Something(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public int ReturnSomething()
        {
            return a * b / c;
        }
        public override string ToString()
        {
            if (a == 0 && b == 0 && c == 0) return "Nulls";
            return "Something:" + a + "-" + b + "-" + c;
        }
    }
}
