﻿using NUnit.Framework;
using AutomationYandexMail.Framework;

namespace AutomationYandexMail.Tests
{
    class BaseTest
    {
        private const string baseUrl = "http://www.yandex.by";

        [SetUp]
        public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);

        [TearDown]
        public void CloseBrowser() => Browser.Instance.Close();
    }
}
