﻿using NUnit.Framework;
using AutomationYandexMail.Services;
using AutomationYandexMail.Models;

namespace AutomationYandexMail.Tests
{
    [TestFixture]
    class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();

        private const string expected_text_of_compose_button_in_case_of_correct_account = "Написать";
        private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

        //Positive Test
        [Test]
        public void LoginTest_LoginWithCorrectAccount_NoErrorMessage()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            string actualTextOfComposeButton = loginService.RetrieveTextOfComposeButton();
            Assert.That(expected_text_of_compose_button_in_case_of_correct_account, Is.EqualTo(actualTextOfComposeButton));
        }

        //Negative Tests
        [Test]
        public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.NonExistedAccount);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
        }

        [Test]
        public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(AccountFactory.AccountWithWrongPassword);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
        }
    }
}
