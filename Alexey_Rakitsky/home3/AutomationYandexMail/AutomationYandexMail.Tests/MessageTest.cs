﻿using NUnit.Framework;
using AutomationYandexMail.Services;
using AutomationYandexMail.Models;

namespace AutomationYandexMail.Tests
{
    [TestFixture]
    class MessageTest : BaseTest
    {
        LoginService loginService = new LoginService();
        MessageService messageService = new MessageService();

        //Tests
        [Test]
        public void ReceivingMessageTest_CreateAndSendMessage_MessageIsInInboxAndSent()
        {
            loginService.LoginToMailBox(AccountFactory.Account);

            Message testMessage = MessageFactory.NewMessage;
            messageService.CreateNewMessage(testMessage);
            
            Assert.True(messageService.FindMessageInInbox(testMessage));
            Assert.True(messageService.FindMessageInSent(testMessage));
        }

        [Test]
        public void DeleteMessageTest_SendReceiveDeleteMessage_MessageIsInTrash()
        {
            loginService.LoginToMailBox(AccountFactory.Account);

            Message testMessage = MessageFactory.NewMessage;
            messageService.CreateNewMessage(testMessage);

            Assert.True(messageService.DeleteMessageInTrash(testMessage));
        }

        [Test]
        public void PermanentlyDeleteMessageTest_SendReceiveDeleteFromTrashMessage_MessageIsNotInTrash()
        {
            loginService.LoginToMailBox(AccountFactory.Account);

            Message testMessage = MessageFactory.NewMessage;
            messageService.CreateNewMessage(testMessage);

            Assert.True(messageService.DeleteMessageInTrash(testMessage));
            Assert.True(messageService.DeleteMessageFromTrash(testMessage));
        }
    }
}
