﻿using System;

namespace AutomationYandexMail.Models
{
    public class MessageFactory
    {
        private const string defaultSendEmail = "alexeyzhv@yandex.ru";
        private const string defaultMessageTheme = "Test Message Theme";
        private const string defaultMessageText = "Test Message Text";

        private static readonly string MESSAGE_THEME = defaultMessageTheme + new Random().Next();
        private static readonly string MESSAGE_TEXT = defaultMessageText + new Random().Next();

        public static Message NewMessage
        {
            get { return new Message(defaultSendEmail, MESSAGE_THEME, MESSAGE_TEXT); }
        }
    }
}
