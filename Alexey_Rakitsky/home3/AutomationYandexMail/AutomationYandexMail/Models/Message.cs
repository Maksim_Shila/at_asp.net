﻿namespace AutomationYandexMail.Models
{
    public class Message
    {
        public string SendEmail { get; set; }
        public string MessageTheme { get; }
        public string MessageText { get; }

        public Message(string sendEmail, string messageTheme, string messageText)
        {
            SendEmail = sendEmail;
            MessageTheme = messageTheme;
            MessageText = messageText;
        }
    }
}
