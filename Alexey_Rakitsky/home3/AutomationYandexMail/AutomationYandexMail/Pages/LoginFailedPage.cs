﻿using AutomationYandexMail.Framework;
using OpenQA.Selenium;

namespace AutomationYandexMail.Pages
{
    class LoginFailedPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By errorMessageByCss = By.CssSelector(".error-msg");

        internal IWebElement ErrorMessage { get { return browser.FindElement(errorMessageByCss); } }
    }
}
