﻿using AutomationYandexMail.Models;
using AutomationYandexMail.Pages;

namespace AutomationYandexMail.Services
{
    public class LoginService
    {
        private MailLoginPage mailLoginPage = new MailLoginPage();

        public void LoginToMailBox(UserAccount account)
        {
            mailLoginPage.LoginInput.SendKeys(account.Login);
            mailLoginPage.PasswordInput.SendKeys(account.Password);
            mailLoginPage.SubmitButton.Click();
        }

        public string RetrieveErrorOnFailedLogin()
        {
            return new LoginFailedPage().ErrorMessage.Text;
        }

        public string RetrieveTextOfComposeButton()
        {
            return new MailAccountPage().ComposeButton.Text;           
        }
    }
}
