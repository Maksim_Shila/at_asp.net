﻿using AutomationYandexMail.Models;
using AutomationYandexMail.Pages;
using OpenQA.Selenium;
using System.Threading;

namespace AutomationYandexMail.Services
{
    public class MessageService
    {
        private MailAccountPage mailAccountPage = new MailAccountPage();
        private NewMessagePage newMessagePage = new NewMessagePage();

        public void CreateNewMessage(Message message)
        {
            mailAccountPage.ComposeButton.Click();
            newMessagePage.SendEmail.SendKeys(message.SendEmail);
            newMessagePage.MessageTheme.SendKeys(message.MessageTheme);
            newMessagePage.MessageText.SendKeys(message.MessageText);
            newMessagePage.SendButton.Click();
            Thread.Sleep(2000);
        }

        public bool FindMessageInInbox(Message message)
        {
            IWebElement item = null;
            mailAccountPage.InboxButton.Click();
            Thread.Sleep(1000);
            mailAccountPage.CheckButton.Click();
            Thread.Sleep(1000);
            try
            {
                item = mailAccountPage.Message(message.MessageTheme);
            }
            catch
            {
            }
            return (item != null);
        }

        public bool FindMessageInSent(Message message)
        {
            IWebElement item = null;
            mailAccountPage.SentButton.Click();
            Thread.Sleep(1000);
            mailAccountPage.CheckButton.Click();
            Thread.Sleep(1000);
            try
            {
                item = mailAccountPage.Message(message.MessageTheme);
            }
            catch
            {
            }
            return (item != null);
        }

        public bool FindMessageInTrash(Message message)
        {
            IWebElement item = null;
            mailAccountPage.TrashButton.Click();
            Thread.Sleep(1000);
            mailAccountPage.CheckButton.Click();
            Thread.Sleep(1000);
            try
            {
                item = mailAccountPage.Message(message.MessageTheme);
            }
            catch
            {
            }
            return (item != null);
        }

        public bool DeleteMessageInTrash(Message message)
        {
            mailAccountPage.InboxButton.Click();
            Thread.Sleep(1000);
            mailAccountPage.CheckButton.Click();
            Thread.Sleep(1000);
            try
            {
                mailAccountPage.Message(message.MessageTheme);
                Thread.Sleep(1000);
                mailAccountPage.GetMarkMessageCheckbox(message.MessageTheme).Click();
                Thread.Sleep(1000);
                if (mailAccountPage.DeleteButton.Enabled)
                {
                    mailAccountPage.DeleteButton.Click();
                }
                return (!FindMessageInSent(message));
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMessageFromTrash(Message message)
        {
            mailAccountPage.TrashButton.Click();
            Thread.Sleep(1000);
            mailAccountPage.CheckButton.Click();
            Thread.Sleep(1000);
            try
            {
                while (FindMessageInTrash(message))
                {
                    mailAccountPage.Message(message.MessageTheme);
                    Thread.Sleep(1000);
                    mailAccountPage.GetMarkMessageCheckbox(message.MessageTheme).Click();
                    Thread.Sleep(1000);
                    if (mailAccountPage.DeleteButton.Enabled)
                    {
                        mailAccountPage.DeleteButton.Click();
                    }
                }
                return (!FindMessageInTrash(message));
            }
            catch
            {
                return false;
            }
        }
    }
}
